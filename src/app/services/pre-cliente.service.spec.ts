import { TestBed } from '@angular/core/testing';

import { PreClienteService } from './pre-cliente.service';

describe('PreClienteService', () => {
  let service: PreClienteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreClienteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
