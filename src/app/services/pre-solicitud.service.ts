import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';


const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PreSolicitudService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }

  registrarPreSolicitud(datos: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>(`${apiUrl}sc/guardar_pre_solicitud`, JSON.stringify(datos), { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }

  cargarDistinoCredito() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}cr/Listar_Cat_Credito?obj=destino_credito&search=all&ident=0`, { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }

  cargarDatosProducto() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}cr/Listar_Cat_Credito?obj=carg_productos&search=all&ident=0`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  cargarDatosFrecuencia() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}cr/Listar_Cat_Credito?obj=frecuencia_pago&search=all&ident=0`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }


}
