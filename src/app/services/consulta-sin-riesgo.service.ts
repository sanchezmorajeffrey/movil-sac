import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ConsultaSinRiesgoService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }


  // tslint:disable-next-line: variable-name
  consultaRecord(numero_identificacion: string, tipo_persona: string, tipo_cosulta: number) {
    const data = { numero_identificacion, tipo_persona, tipo_cosulta };
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>(`${apiUrl}sac/consulta_record`, data, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  guardarConsultaRecord(data: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>
      (`${apiUrl}sc/guardar_consulta`, JSON.stringify(data), { headers: h }).pipe(map((res) => {
        console.log('consulta guardada');
        return res;
      }));
  }


  guardarSeccionSolicitud(idPresolicitud: any, tipoCliene: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>
    (`${apiUrl}sc/cargar_seccion_solicitud?id_pre_solicitud=${idPresolicitud}&tipo_cliente=${tipoCliene}`, { headers: h })
    .pipe(map((res) => {
      return res;
    }));
  }
}
