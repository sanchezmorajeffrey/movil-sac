import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';


const apiUrl = environment.apiUrl;


@Injectable({
  providedIn: 'root'
})
export class PersonaJuridicaService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }


  obtenerSeccion(idPresolicitud: any, idSeccion: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/cargar_cuestionario_seccion?id_seccion=${idSeccion}&id_pre_solicitud=${idPresolicitud}`
      , { headers: h }).pipe(map((res) => {
        return res;
      }));
  }
}
