import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }

  getProductos() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}pa/pacredito?obj=producto`, { headers: h }).pipe(map((res) => {
      console.log('Productos desde el servicio', res);
      return res;
    }));
  }

  getAseguradora() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}pa/pacredito?obj=aseguradora`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  // tslint:disable-next-line: variable-name
  getCuota(monto: any, codigo_producto: any, plazo: any, codigo_aseguradora: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}cr/calcular_cuota?monto=${monto}&codigo_producto=${codigo_producto}&plazo=${plazo}&codigo_aseguradora=${codigo_aseguradora}`,
      { headers: h }).pipe(map((res) => {
        return res;
      }));
  }
}
