import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Storage } from '@ionic/storage';
import { NavController, } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';


const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  token: string = null;
  currentUser: any;
  menuEncabezado: any;
  menuDetalle: any;
  constructor(private http: HttpClient, private router: Router, private storage: Storage, private navCtrl: NavController) { }

  login(username: string, password: string) {
    const data = null;
    password = this.Encry(password).toString();
    console.log('password', password);
    username = this.Encry(username).toString();
    console.log('username', username);
    const h = new HttpHeaders().set('Content-Type', 'application/json').set('username', username).set('password', password).set('id_aplicacion', 'MOV');
    console.log(h);
    return this.http.post<any>(`${apiUrl}login/authenticate`, data, { headers: h }).pipe(map(user => {
      console.log(user[0].keytoken);
      this.guardarToken(user[0].keytoken);
      this.guardarMenuDetalle(user[0].menu_det_par);
      this.guardarMenuEncabezado(user[0].menu_enca_par);
      return user;
    }));
  }

  async guardarToken(token: string) {
    this.token = token;
    await this.storage.set('token', token);
    await this.validaToken();
  }

  async guardarMenuEncabezado(menu: any) {
    this.menuEncabezado = menu;
    await this.storage.set('menuenca', menu);
  }

  async guardarMenuDetalle(menu: any) {
    this.menuDetalle = menu;
    await this.storage.set('menude', menu);
  }

  async cargarToken() {
    this.token = await this.storage.get('token') || null;
  }



  async validaToken(): Promise<boolean> {

    await this.cargarToken();
    if (!this.token) {
      this.navCtrl.navigateRoot('/login');
      return Promise.resolve(false);
    } else {
      return Promise.resolve(true);
    }

  }

  Encry(text) {
    if (text !== '') {
      const key = CryptoJS.enc.Utf8.parse('hf8685nfhfhjs9h8');
      const iv = CryptoJS.enc.Utf8.parse('8080808080808080');
      const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
        {
          keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
        });
      return encrypted;
    }
  }

  logout() {
    this.token = null;
    this.storage.clear();
    this.navCtrl.navigateRoot('/login');
    // this.router.navigateByUrl('/login');
  }
}
