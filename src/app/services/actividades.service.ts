import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';


const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ActividadesService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }


  obtenerSeccion(idPresolicitud: any, idSeccion: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/cargar_cuestionario_seccion?id_seccion=${idSeccion}&id_pre_solicitud=${idPresolicitud}`
      , { headers: h }).pipe(map((res) => {
        return res;
      }));
  }

  obtenerSectorEconomico() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sac/SC_LISTAR_CAT?obj=1&search=all&ident=1`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  obtenerActividad() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sac/SC_LISTAR_CAT?obj=2&search=all&ident=0`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  obtenerSubActividad(id: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sac/SC_LISTAR_CAT?obj=3&search=all&ident=${id}`, { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  guardarRespuestaActividad(data: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>(`${apiUrl}sc/insertar_respuesta_cuestionario`, JSON.stringify(data), { headers: h }).pipe(map((res) => {
      return res;
    }));
  }

  guardarSeccionTerminada(idPresolicitud: any, idSeccion?: any) {
    const data = {};
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>(`${apiUrl}sc/finalizar_seccion?id_seccion=${idSeccion}&id_pre_solicitud=${idPresolicitud}`
      , JSON.stringify(data), { headers: h }).pipe(map((res) => {
        return res;
      }));
  }

  guardarSeccionSolicitud(idPresolicitud: any, tipoCliene: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>
    (`${apiUrl}sc/cargar_seccion_solicitud?id_pre_solicitud=${idPresolicitud}&tipo_cliente=${tipoCliene}`, { headers: h })
    .pipe(map((res) => {
      return res;
    }));
  }


}
