import { TestBed } from '@angular/core/testing';

import { ConsultaSinRiesgoService } from './consulta-sin-riesgo.service';

describe('ConsultaSinRiesgoService', () => {
  let service: ConsultaSinRiesgoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsultaSinRiesgoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
