import { TestBed } from '@angular/core/testing';

import { PreSolicitudService } from './pre-solicitud.service';

describe('PreSolicitudService', () => {
  let service: PreSolicitudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreSolicitudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
