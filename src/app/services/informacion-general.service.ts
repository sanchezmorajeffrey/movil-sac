import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';


const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class InformacionGeneralService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }

  cargarPreguntasNatural() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/cargar_cuestionario_seccion?id_seccion=2`, { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }

  cargarPreguntasJuridica() {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/cargar_cuestionario_seccion?id_seccion=3`, { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }


  cargarCatalogo(id: any, search = 'all', ident = 0) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sac/SC_LISTAR_CAT?obj=${id}&search=${search}&ident=${ident}`, { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }

  obtenerSeccion(idPresolicitud: any, idSeccion: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/cargar_cuestionario_seccion?id_seccion=${idSeccion}&id_pre_solicitud=${idPresolicitud}`
      , { headers: h }).pipe(map((res) => {
        return res;
      }));
  }

}
