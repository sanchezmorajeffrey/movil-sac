import { TestBed } from '@angular/core/testing';

import { PersonaJuridicaService } from './persona-juridica.service';

describe('PersonaJuridicaService', () => {
  let service: PersonaJuridicaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonaJuridicaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
