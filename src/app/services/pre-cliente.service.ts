import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';


const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PreClienteService {

  constructor(private http: HttpClient, private authenticacionService: AuthenticationService) { }


  registrarPreCliente(datos: any) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.post<any>(`${apiUrl}sc/guardar_pre_cliente`, JSON.stringify(datos), { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }


  buscarClienteCedula(valor: string) {
    const token = this.authenticacionService.token;
    const h = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/json');
    return this.http.get<any>(`${apiUrl}sc/buscar_pre_cliente?identidad=${valor}`, { headers: h }).
      pipe(map((res) => {
        return res;
      }));
  }
}
