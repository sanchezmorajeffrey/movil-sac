import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuController, IonSlides } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertController } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  slideOpts = {
    allowSlidePrev: false,
    allowSlideNext: false,
  };
  submitted = false;
  loginForm: FormGroup;
  returnUrl: string;
  messageError: undefined;
  isLoading = false;
  passwordTypeInput = 'password';
  @ViewChild('slidePrincipal', { static: true }) slides: IonSlides;
  @ViewChild('passwordEyeRegister', { read: ElementRef }) passwordEye: ElementRef;
  constructor(private formBuilder: FormBuilder,
              public menuCtrl: MenuController,
              public alertController: AlertController,
              public router: Router,
              public loadingController: LoadingController,
              private authenticationService: AuthenticationService,
              private navCtrl: NavController, private route: ActivatedRoute,
              public toastController: ToastController) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
  }

  get f() { return this.loginForm.controls; }

  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      translucent: true,
      showBackdrop: false,
      message: 'Iniciando Sesión',
      cssClass: 'custom-loader-class',
    }
    ).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async dismiss() {
    if (this.isLoading) {
      this.isLoading = false;
      return await this.loadingController.dismiss();
    }
    return null;
  }


  login() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.present();
    this.authenticationService.login(this.f.username.value, this.f.password.value).pipe(first())
      .subscribe(async data => {
        console.log(data);
        this.navCtrl.navigateRoot([this.returnUrl], { animated: true });
        this.dismiss();
      },
        async error => {
          console.log(error);
          console.log('Este es el error', error.error.Mensaje);
          this.messageError = error.error.Mensaje;
          this.dismiss();
          const alert = await this.alertController.create({
            header: 'Error',
            message: this.messageError,
            buttons: ['OK']
          });
          await alert.present();
        });
  }


  togglePasswordMode(e) {
    e.preventDefault();
    this.passwordTypeInput = this.passwordTypeInput === 'text' ? 'password' : 'text';
    const nativeEl = this.passwordEye.nativeElement.querySelector('input');
    const inputSelection = nativeEl.selectionStart;
    nativeEl.focus();
    setTimeout(() => {
      nativeEl.setSelectionRange(inputSelection, inputSelection);
    }, 1);
  }

}
