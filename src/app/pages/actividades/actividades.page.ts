import { Component, OnInit,  Renderer2, ElementRef } from '@angular/core';
import { ActividadesService } from 'src/app/services/actividades.service';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { map, startWith, tap } from 'rxjs/operators';
import { NavController, } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, ValidationErrors, FormControl } from '@angular/forms';




interface Model {
  readonly subActividadEconomica: string;
  readonly sectorEconomico: number;
  readonly actividadEcomica: string;
}

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.page.html',
  styleUrls: ['./actividades.page.scss'],
})
export class ActividadesPage implements OnInit {
  preguntas: any[] = [];
  // sectorEconomico: any[] = [];
  activadadEconomica: any[] = [];
  subActividadEconomica: any[] = [];
  idSubActivad: any;
  formActividad: FormGroup;
  submitted = false;

  selectIdActividad = 0;
  selectIdSubActividad = 0;
  // selectIdSectorEconomico = 0;

  interfaceActividad = 'action-sheet';
  interfaceSubActividad = 'action-sheet';

  requeridoSectorEconomico = false;
  requeridoSubActividad = false;
  requeridoActividad = false;

  idPresolicitud: any;
  idSeccion: any;

  tipoPersona: any;


  idSeccionContinua: any;


  constructor(private router: Router, private ruta: ActivatedRoute,   private renderer: Renderer2,
              private actividad: ActividadesService, private elmRef: ElementRef,
              public alertController: AlertController, public formBuilder: FormBuilder,
              private storage: Storage, private navCtrl: NavController) {
    this.formActividad = this.createFormSolicitud();
  }

  ngOnInit() {
    this.cargarIdPreSolicitud();
    // this.obtenerSectorEconomico();
    this.ruta.params.subscribe(param => {
      this.idSeccion = param.idSeccion;
      this.tipoPersona = param.tipo;
    });
    this.obtenerActividadEconomica();
    console.log('tipo personas', this.tipoPersona);
    console.log('idSeccion', this.idSeccion);
    this.renderer.addClass(this.elmRef.nativeElement, 'crazyClass');
    // this.obtenerPreguntas();
  }

  private createFormSolicitud() {
    return this.formBuilder.group({
      // _SECTOR_ECONOMICO: ['', Validators.compose([Validators.required, this.mayorACero])],
      _ACTIVIDAD_ECONOMICA: ['', Validators.compose([Validators.required, this.mayorACero])],
      _SUB_ACTIVIDAD_ECONOMICA: ['', Validators.compose([Validators.required, this.mayorACero])]
    });
  }


  obtenerPreguntas() {
    this.actividad.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res) => {
      console.log('preguntas', res);
      this.preguntas = res;
    });
  }


  // obtenerSectorEconomico() {
  //   this.actividad.obtenerSectorEconomico().subscribe((res) => {
  //     console.log('sector economica', res);
  //     this.sectorEconomico = res;
  //   });
  // }

  get f() { return this.formActividad.controls; }


  obtenerActividadEconomica() {
    this.actividad.obtenerActividad().subscribe((res) => {
      this.activadadEconomica = res;
      if (this.activadadEconomica.length <= 3) {
        this.interfaceActividad = 'ion-alert';
      } else {
        this.interfaceActividad = 'action-sheet';
      }
    });
  }


  obtenerSubActividadEconomica(id: any) {
    this.actividad.obtenerSubActividad(id).subscribe((res) => {
      this.subActividadEconomica = res;
      if (this.subActividadEconomica.length <= 3) {
        this.interfaceSubActividad = 'ion-alert';
      } else {
        this.interfaceSubActividad = 'action-sheet';
      }
    }, error => {
      this.subActividadEconomica = this.activadadEconomica.
        filter((actividad) => actividad.ID_ACTIVIDA_ECONOMICA === this.idSubActivad);
      this.selectIdSubActividad = this.subActividadEconomica[0].ID_ACTIVIDA_ECONOMICA;
      this.requeridoSubActividad = false;
      if (this.subActividadEconomica.length <= 3) {
        this.interfaceSubActividad = 'ion-alert';
      } else {
        this.interfaceSubActividad = 'action-sheet';
      }
    });
  }


  onChangeSectorEconomico(event: any) {
    this.selectIdActividad = 0;
    this.selectIdSubActividad = 0;
    this.activadadEconomica = [];
    this.subActividadEconomica = [];
    // this.obtenerActividadEconomica(event.target.value);
    this.requeridoSectorEconomico = false;
  }


  onChangeActividad(event: any) {
    this.selectIdSubActividad = 0;
    this.subActividadEconomica = [];

    if (event.target.value !== 0) {
      this.idSubActivad = event.target.value;
      this.obtenerSubActividadEconomica(event.target.value);
      this.requeridoActividad = false;
    }

  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }

  guardar() {
    this.submitted = true;
    if (this.formActividad.invalid) {
      return;
    }

    const data = [
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[0].ID_CUESTIONARIO,
        _REPUESTA: this.formActividad.get('_SECTOR_ECONOMICO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[1].ID_CUESTIONARIO,
        _REPUESTA: this.formActividad.get('_ACTIVIDAD_ECONOMICA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[2].ID_CUESTIONARIO,
        _REPUESTA: this.formActividad.get('_SUB_ACTIVIDAD_ECONOMICA').value,
      }
    ];
    console.log('esto envia', data);
    this.actividad.guardarRespuestaActividad(data).subscribe((res) => {
      console.log('respuesta de guardado', res);
    });
    this.actividad.guardarSeccionTerminada(this.idPresolicitud, this.idSeccion).subscribe((resSeccionTerminada) => {
      console.log('guardado de seccion terminada', resSeccionTerminada);
    });
    console.log('id seccion dentro del metodo guardar');
    this.actividad.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((resSaber) => {
      console.log('endpoint para saber la siguiente id', resSaber);
      this.idSeccionContinua = resSaber[0].ID_SECCION;
      switch (this.idSeccionContinua) {
        case 1:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 2:
          this.navCtrl.navigateRoot(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.navCtrl.navigateRoot(['/informacion-persona-juridica', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 4:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        default:
          break;
      }
    });
  }

  onChangeSubActividad() {
    this.requeridoSubActividad = false;
  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }


  mayorACero(control: FormControl): ValidationErrors {
    // tslint:disable-next-line: radix
    const mayor = parseInt(control.value);

    if (mayor === 0) {
      return { mayorACero: true };
    }
    else {
      return null;
    }
  }


}
