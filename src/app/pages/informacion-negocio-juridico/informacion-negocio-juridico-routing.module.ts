import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionNegocioJuridicoPage } from './informacion-negocio-juridico.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionNegocioJuridicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionNegocioJuridicoPageRoutingModule {}
