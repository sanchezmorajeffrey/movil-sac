import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionNegocioJuridicoPage } from './informacion-negocio-juridico.page';

describe('InformacionNegocioJuridicoPage', () => {
  let component: InformacionNegocioJuridicoPage;
  let fixture: ComponentFixture<InformacionNegocioJuridicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionNegocioJuridicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionNegocioJuridicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
