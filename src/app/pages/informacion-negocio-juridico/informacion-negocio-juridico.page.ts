import { Component, OnInit } from '@angular/core';
import { ActividadesService } from 'src/app/services/actividades.service';
import { FormArray, FormBuilder, FormGroup, Validators, ValidationErrors, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaJuridicaService } from 'src/app/services/persona-juridica.service';

import { Paises } from '../informacion-persona/paises';
import { InformacionGeneralService } from 'src/app/services/informacion-general.service';
import { NavController, } from '@ionic/angular';



@Component({
  selector: 'app-informacion-negocio-juridico',
  templateUrl: './informacion-negocio-juridico.page.html',
  styleUrls: ['./informacion-negocio-juridico.page.scss'],
})
export class InformacionNegocioJuridicoPage implements OnInit {
  formInformacionNegocio: FormGroup;
  idSeccionContinua: any;
  idSeccion: any;
  tipoPersona: any;
  idPresolicitud: any;
  preguntas: any[] = [];
  datosEntidadImpuesto: any[] = [];
  prueba: any;
  selectedLanguage: any;
  submitted = false;

  negocioPropio = false;
  constructor(private router: Router, private ruta: ActivatedRoute,
              private storage: Storage,
              public personaJuridica: PersonaJuridicaService,
              public formBuilder: FormBuilder,
              private route: ActivatedRoute,
              public alertController: AlertController,
              public informacion: InformacionGeneralService,
              private actividad: ActividadesService, private navCtrl: NavController) {
    this.formInformacionNegocio = this.createMyFormInformacionNegocio();
  }

  ngOnInit() {
    this.ruta.params.subscribe(param => {
      this.idSeccion = param.idSeccion;
      this.tipoPersona = param.tipo;
      console.log('idSeccion', this.idSeccion);
      console.log('tipoPersoona', this.tipoPersona);
    });
    this.cargarIdPreSolicitud();
  }

  obtenerPreguntas() {
    this.personaJuridica.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res: any) => {
      console.log('preguntas', res);
      this.preguntas = res;

      this.preguntas.forEach(element => {
        if (element.TIPO === 'Multiple Seleccion') {
          if (element.ID_CUESTIONARIO === 98) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosEntidadImpuesto = resCatalogo;
              console.log('entidadImpuesto', this.datosEntidadImpuesto);
            });
          }
        }
      });
    });

  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }

  private createMyFormInformacionNegocio() {
    return this.formBuilder.group({
      _TIEMPO_EXISTENCIA: ['',  Validators.required],
      _COMO_EMPEZO_NEGOCIO: ['', Validators.required],
      _LOCAL_PROPIO: ['', Validators.required],
      _TIEMPO_PERMANENCIA_ALQUILER: [''],
      _ANOS_EXPERIENCIA: ['', Validators.required],
      _CANTIDAD_EMPLEADO: ['', Validators.required],
      _CANTIDAD_NEGOCIO_MERCADO: ['', Validators.required],
      _CAPACIDAD_EXTENDER: ['', Validators.required],
      _MATRICULA_PERMISO: ['', Validators.required],
      _LISTAR_ENTIDADES_IMPUESTO: ['', Validators.required],
      _OBLIGACIONES_ENTIDAD: ['', Validators.required]
    });
  }

  guardarInformacionNegocio() {
    this.submitted = true;
    if (this.formInformacionNegocio.invalid) {
      return;
    }
    const dat = this.formInformacionNegocio.get('_LISTAR_ENTIDADES_IMPUESTO').value;
    console.log('dat', dat);
    let multiple: any;
    const multipl: any[] = [];
    if (dat.length > 0) {
      dat.forEach(element => {
        multiple = [{
          _ID_PRE_SOLICITUD: this.idPresolicitud,
          _ID_CUESTIONARIO: this.preguntas[9].ID_CUESTIONARIO,
          _REPUESTA: element
        }];
        multipl.push(...multiple);
      });
    }

    const data = [
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[0].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_TIEMPO_EXISTENCIA').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[1].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_COMO_EMPEZO_NEGOCIO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[2].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_LOCAL_PROPIO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[3].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[4].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_ANOS_EXPERIENCIA').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[5].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_CANTIDAD_EMPLEADO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[6].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_CANTIDAD_NEGOCIO_MERCADO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[7].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_CAPACIDAD_EXTENDER').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[8].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_MATRICULA_PERMISO').value
      },
      ...multipl,
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[10].ID_CUESTIONARIO,
        _REPUESTA: this.formInformacionNegocio.get('_OBLIGACIONES_ENTIDAD').value
      }
    ];

    this.actividad.guardarRespuestaActividad(data).subscribe((res) => {
      console.log('respuesta de guardado', res);
    });
    this.actividad.guardarSeccionTerminada(this.idPresolicitud, this.idSeccion).subscribe((resSeccionTerminada) => {
      console.log('guardado de seccion terminada', resSeccionTerminada);
    });
    console.log('id seccion dentro del metodo guardar', );
    this.actividad.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((resSaber) => {
      console.log('endpoint para saber la siguiente id', resSaber);
      this.idSeccionContinua = resSaber[0].ID_SECCION;
      switch (this.idSeccionContinua) {
        case 1:
          this.router.navigate(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 2:
          this.router.navigate(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.router.navigate(['/informacion-persona-juridica', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 4:
          this.router.navigate(['/actividades', this.tipoPersona]);
          break;
        case 5:
          this.router.navigate(['/actividades', this.tipoPersona]);
          break;
        case 6:
          this.router.navigate(['/informacion-negocio-juridico', this.tipoPersona, this.idSeccionContinua]);
          break;
        default:
          break;
      }
    });
    console.log('Data del formulario', data);
  }


  changeTiempo(event: any) {
    console.log(event.detail.value);
    const mostrar = event.detail.value;

    if (mostrar === 'Sí') {
      this.negocioPropio = true;
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').reset();
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').clearValidators();
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').setValidators([Validators.required]);
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').updateValueAndValidity();
    } else {
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').reset();
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').clearValidators();
      this.formInformacionNegocio.get('_TIEMPO_PERMANENCIA_ALQUILER').updateValueAndValidity();
      this.negocioPropio = false;
    }
  }

  get f() { return this.formInformacionNegocio.controls; }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }


   mayorACero(control: FormControl): ValidationErrors {
      // tslint:disable-next-line: radix
      const mayor = parseInt(control.value);

      if (mayor < 0) {
        return null;
      }
      else {
          return { mayorACero: true };
      }
  }

}
