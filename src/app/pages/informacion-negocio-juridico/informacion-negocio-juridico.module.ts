import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacionNegocioJuridicoPageRoutingModule } from './informacion-negocio-juridico-routing.module';

import { InformacionNegocioJuridicoPage } from './informacion-negocio-juridico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    InformacionNegocioJuridicoPageRoutingModule
  ],
  declarations: [InformacionNegocioJuridicoPage]
})
export class InformacionNegocioJuridicoPageModule {}
