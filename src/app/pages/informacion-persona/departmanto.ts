export const data = {
    departamento: {
        id: 'DI1',
        label: 'Seleccione departamento',
        value: '',
        type: 'select',
        validation: {
            required: true
        },
        opciones: [
            { id: 1, valor: 'Masaya' },
            { id: 1, valor: 'Granada' }
        ]
    },
    municipio: {
        label: 'Seleccione municipio',
        value: '',
        type: 'select',
        validation: {
            required: true
        },
        cond: 'id.DT1.id === id',
        opciones: [
            { id: 1, idDepartamento: 1, valor: 'Masaya' },
            { id: 2, idDepartamento: 1, valor: 'Catarina' },
            { id: 3, idDepartamento: 1, valor: 'Masatepe' },
            { id: 4, idDepartamento: 1, valor: 'Nandasmo' },
            { id: 5, idDepartamento: 1, valor: 'Nindiri' },
            { id: 6, idDepartamento: 1, valor: 'Niquinomo' },
            { id: 7, idDepartamento: 1, valor: 'San juan de oriente' },
            { id: 8, idDepartamento: 1, valor: 'tiesma' },
            { id: 9, idDepartamento: 2, valor: 'Diría' },
            { id: 10, idDepartamento: 2, valor: 'Diriomo' },
            { id: 11, idDepartamento: 2, valor: 'Granada' },
            { id: 12, idDepartamento: 2, valor: 'Nandaime' },
        ]
    }
};
