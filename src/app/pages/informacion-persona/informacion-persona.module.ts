import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacionPersonaPageRoutingModule } from './informacion-persona-routing.module';

import { InformacionPersonaPage } from './informacion-persona.page';

import { BrMaskerModule } from 'br-mask';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BrMaskerModule,
    InformacionPersonaPageRoutingModule
  ],
  declarations: [InformacionPersonaPage]
})
export class InformacionPersonaPageModule {}
