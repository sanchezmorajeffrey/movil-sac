export const Preguntas = [
    {
        DEPENDENCIA: 0,
        DESCRIPCION_CATEGORIA: 'Información del Cliente',
        DESCRIPCION_SECCION: 'Información PN',
        ID_CATEGORIA: 2,
        ID_CUESTIONARIO: 4,
        ID_SECCION: 2,
        PREGUNTA: 'Dirección Exacta',
        TIPO: 'Texto',
        UNIDAD_MEDIDA: null,
        VALOR_DEPENDENCIA: '-',
        MAXLENGTH: 20,
        MENSAJEVALIDACION: 'requerido',
    },
    {
        DEPENDENCIA: 0,
        DESCRIPCION_CATEGORIA: 'Información del Cliente',
        DESCRIPCION_SECCION: 'Información PN',
        ID_CATEGORIA: 2,
        ID_CUESTIONARIO: 6,
        ID_SECCION: 2,
        PREGUNTA: 'Municipio',
        TIPO: 'Catalogo',
        UNIDAD_MEDIDA: null,
        VALOR_DEPENDENCIA: '-',
        MAXLENGTH: 20,
        MENSAJEVALIDACION: 'requerido',
        RESPUESTA: [
            {
                ID: '1',
                VALOR: '1'
            }
        ]
    },
    {
        required: [
            'name',
            'email',
            'comment'
        ]
    }

];
