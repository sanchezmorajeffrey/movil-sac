import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { InformacionGeneralService } from 'src/app/services/informacion-general.service';
import { Storage } from '@ionic/storage';
import { Paises } from '../informacion-persona/paises';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, } from '@ionic/angular';
import { ActividadesService } from 'src/app/services/actividades.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-informacion-persona',
  templateUrl: './informacion-persona.page.html',
  styleUrls: ['./informacion-persona.page.scss'],
})
export class InformacionPersonaPage implements OnInit {
  tipoPersona: any;
  submitted = false;
  formPersonaNatural: FormGroup;
  preguntasNatural: any[] = [];
  dataCargarCatalogo: any[] = [];
  respuestaCatalogo: any;
  datosCivil: any[] = [];
  idPresolicitud: any;
  datosDepartamento: any[] = [];
  datosMunicipio: any[] = [];
  datosEnfermedad: any[] = [];
  idDepartamento: any;
  relacionPariente: any[] = [];
  idSeccion: any;
  selectIdMunicipio = 0;
  esCasado = false;
  alquiler = false;
  contador = '';
  mascara = { mask: '000-000000-00000', len: 16, userCaracters: true };
  estadoEnfermo = false;
  servicioBasico = false;
  idSeccionContinua: any;


  constructor(private router: Router, private storage: Storage,
              private navCtrl: NavController, private actividad: ActividadesService,
              private route: ActivatedRoute, public informacion: InformacionGeneralService, public formBuilder: FormBuilder,
              public alertController: AlertController) {
    this.formPersonaNatural = this.createMyFormPersonaNatural();

  }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.tipoPersona = param.tipo;
      this.idSeccion = param.idSeccion;
    });
    console.log('tipo persona', this.tipoPersona);
    this.cargarIdPreSolicitud();
  }

  private createMyFormPersonaNatural() {
    return this.formBuilder.group({
      _DIRECCION_EXACTA: ['', Validators.required],
      _DEPARTAMENTO: ['', Validators.required],
      _MUNICIPIO: ['', Validators.required],
      _EMAIL: ['', Validators.compose([Validators.pattern('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$')])],
      _ESTADO_CIVIL: ['', Validators.required],
      // DATOS SI ES CASADO
      _NUMERO_IDENTIFICACION: [''],
      _PRIMER_NOMBRE_CONYUGUE: [''],
      _SEGUNDO_NOMBRE_CONYUGUE: [''],
      _PRIMER_APELLIDO_CONYUGUE: [''],
      _SEGUNDO_APELLIDO_CONYUGUE: [''],
      _TELEFONO_CELULAR_CONYUGUE: [''],
      _TELEFONO_CONVENCIONA_CONYUGUE: [''],
      _EMAIL_CONYUGUE: [''],
      //

      _ES_ALQUILADO: ['', Validators.required],
      _TIEMPO_RESIDIR: [''],
      _DEPENDIENTES: ['', Validators.required],
      _DEPENDIENTES_ESCOLARES: ['', Validators.required],
      _ES_ENFERMO: ['', Validators.required],
      _SELECION_ENFERMEDAD: [''],
      _TIENE_FAMILIAR_PROBLEMA: ['', Validators.required],
      _SERVICIOS_BASICO: ['', Validators.required],
      _NOMBRE_PERSONA_BASICO: [''],
      _RELACION_PERSONA_SERVICO_BASICO: ['']
    });
  }

  changeEnfermo(event: any) {
    console.log('enfermon', event.target.value);
    const estadoEnfermo = event.target.value;

    if (estadoEnfermo === 'Sí') {
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').reset();
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').clearValidators();
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').setValidators([Validators.required]);
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').updateValueAndValidity();
      this.estadoEnfermo = true;
    }
    else {
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').reset();
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').clearValidators();
      this.formPersonaNatural.get('_SELECION_ENFERMEDAD').updateValueAndValidity();
      this.estadoEnfermo = false;
    }
  }


  changeEnfermoPariente(event: any) {
    console.log('enfermo pariente', event.target.value);
  }

  changeServicioBasicos(event: any) {
    console.log('servicioBasico', event.target.value);
    const servicioBasico = event.target.value;
    if (servicioBasico === 'No') {
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').reset();
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').clearValidators();
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').setValidators([Validators.required]);
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').updateValueAndValidity();


      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').reset();
      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').clearValidators();
      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').setValidators([Validators.required]);
      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').updateValueAndValidity();

      this.servicioBasico = true;
    }
    else {
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').reset();
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').clearValidators();
      this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').updateValueAndValidity();


      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').reset();
      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').clearValidators();
      this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').updateValueAndValidity();

      this.servicioBasico = false;
    }
  }



  obtenerPreguntas() {
    this.informacion.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res) => {
      this.preguntasNatural = res;
      console.log('preguntas natural', this.preguntasNatural);
      this.preguntasNatural.forEach(element => {
        if (element.TIPO === 'Catalogo') {
          if (element.ID_CUESTIONARIO === 5) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosDepartamento = resCatalogo;
              console.log('departamento', this.datosDepartamento);
            });
          }
          if (element.ID_CUESTIONARIO === 8) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosCivil = resCatalogo;
              console.log('estado civil', this.datosCivil);
            });
          }
          if (element.ID_CUESTIONARIO === 22) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosEnfermedad = resCatalogo;
              console.log('enfermedad', this.datosEnfermedad);
            });
          }
          if (element.ID_CUESTIONARIO === 26) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.relacionPariente = resCatalogo;
              console.log('relacionPariente', this.relacionPariente);
            });
          }
        }
      });
    });
  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }


  guardarPersonaNatural() {
    this.submitted = true;
    if (this.formPersonaNatural.invalid) {
      return;
    }

    const data = [
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[0].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_DIRECCION_EXACTA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[1].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_DEPARTAMENTO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[2].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_MUNICIPIO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[3].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_EMAIL').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[4].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_ESTADO_CIVIL').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[5].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[6].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[7].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_SEGUNDO_NOMBRE_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[8].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_SEGUNDO_APELLIDO_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[9].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[10].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_TELEFONO_CONVENCIONA_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[12].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_EMAIL_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[13].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_ES_ALQUILADO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[14].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_TIEMPO_RESIDIR').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[15].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_DEPENDIENTES').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[16].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_DEPENDIENTES_ESCOLARES').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[17].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_ES_ENFERMO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[18].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_SELECION_ENFERMEDAD').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[19].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_TIENE_FAMILIAR_PROBLEMA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[20].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_SERVICIOS_BASICO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[21].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_NOMBRE_PERSONA_BASICO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasNatural[22].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaNatural.get('_RELACION_PERSONA_SERVICO_BASICO').value,
      }
    ];

    console.log('paso las validaciones', this.formPersonaNatural.value);
    this.actividad.guardarRespuestaActividad(data).subscribe((res) => {
      console.log('respuesta de guardado', res);
    });
    this.actividad.guardarSeccionTerminada(this.idPresolicitud, this.idSeccion).subscribe((resSeccionTerminada) => {
      console.log('guardado de seccion terminada', resSeccionTerminada);
    });
    console.log('id seccion dentro del metodo guardar');
    this.actividad.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((resSaber) => {
      console.log('endpoint para saber la siguiente id', resSaber);
      this.idSeccionContinua = resSaber[0].ID_SECCION;
      switch (this.idSeccionContinua) {
        case 1:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 2:
          this.navCtrl.navigateRoot(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.navCtrl.navigateRoot(['/informacion-persona-juridica', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 4:
          this.navCtrl.navigateRoot(['/economia-familiar', this.tipoPersona, this.idSeccionContinua]);
          break;
        default:
          break;
      }
    });
  }

  get _EMAIL() {
    return this.formPersonaNatural.get('_EMAIL');
  }

  get _EMAIL_CONYUGUE() {
    return this.formPersonaNatural.get('_EMAIL_CONYUGUE');
  }

  get f() { return this.formPersonaNatural.controls; }


  onChangeDepartamentoJuridico(event: any) {
    this.selectIdMunicipio = 0;
    this.datosMunicipio = [];
    this.idDepartamento = event.target.value;
    this.informacion.cargarCatalogo(this.preguntasNatural[2].ID_CUESTIONARIO, 'all', this.idDepartamento).subscribe((resCatalogo) => {
      this.datosMunicipio = resCatalogo;
      console.log('municipio', this.datosMunicipio);
    });
  }

  onChangeEstadoCivil(event: any) {
    console.log('estado civil', event.target.value);
    const estado = event.target.value;
    if (estado === 'C') {
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').reset();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').clearValidators();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').setValidators([Validators.required, Validators.pattern('^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[A-Z])$')]);
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').updateValueAndValidity();

      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').reset();
      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').setValidators([Validators.required]);
      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').updateValueAndValidity();

      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').reset();
      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').setValidators([Validators.required]);
      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').updateValueAndValidity();


      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').reset();
      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').clearValidators();
      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').setValidators([Validators.required]);
      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').updateValueAndValidity();

      this.formPersonaNatural.get('_EMAIL_CONYUGUE').reset();
      this.formPersonaNatural.get('_EMAIL_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_EMAIL_CONYUGUE').setValidators([Validators.compose([Validators.pattern('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$')])]);
      this.formPersonaNatural.get('_EMAIL_CONYUGUE').updateValueAndValidity();



      // this.mascaraCedulaRepresentante = { mask: '000-000000-00000', len: 16, userCaracters: true };
      this.esCasado = true;
    } else {
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').reset();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').clearValidators();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').updateValueAndValidity();

      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').reset();
      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_PRIMER_NOMBRE_CONYUGUE').updateValueAndValidity();

      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').reset();
      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_PRIMER_APELLIDO_CONYUGUE').updateValueAndValidity();


      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').reset();
      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').clearValidators();
      // this.formPersonaNatural.get('_TELEFONO_CELULAR_CONYUGUE').updateValueAndValidity();

      this.formPersonaNatural.get('_EMAIL_CONYUGUE').reset();
      this.formPersonaNatural.get('_EMAIL_CONYUGUE').clearValidators();
      this.formPersonaNatural.get('_EMAIL_CONYUGUE').updateValueAndValidity();

      this.esCasado = false;
    }

  }

  get _NO_IDENTIFICACION_CONYUGUE() {
    return this.formPersonaNatural.get('_NUMERO_IDENTIFICACION');
  }

  cedulaConyugue(event: any) {
    event.preventDefault();
    console.log('event', event.target.value);
    this.contador = event.target.value;
    this.contador = this.contador.toString();
    if (this.contador.length === 14) {
      console.log('cedula nacional');
      this.mascara = { mask: '000-000000-00000', len: 16, userCaracters: true };
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').clearValidators();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').setValidators(Validators.compose([Validators.required,
      Validators.maxLength(18),
      Validators.pattern('^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[A-Z])$')]));
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').updateValueAndValidity();
    } else {
      console.log('cedula residente');
      this.mascara = { mask: '000000000000', len: 16, userCaracters: true };
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').clearValidators();
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').setValidators([]);
      this.formPersonaNatural.get('_NUMERO_IDENTIFICACION').updateValueAndValidity();
    }
  }


  changeAlquiler(event: any) {
    console.log(event.target.value);

    const alquiler = event.target.value;

    if (alquiler === 'Sí') {
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').reset();
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').clearValidators();
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').setValidators([Validators.required]);
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').updateValueAndValidity();
      this.alquiler = true;
    }
    else {
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').reset();
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').clearValidators();
      this.formPersonaNatural.get('_TIEMPO_RESIDIR').updateValueAndValidity();
      this.alquiler = false;
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }

}


