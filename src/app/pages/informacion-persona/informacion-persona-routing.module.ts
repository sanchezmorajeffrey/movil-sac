import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionPersonaPage } from './informacion-persona.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionPersonaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionPersonaPageRoutingModule {}
