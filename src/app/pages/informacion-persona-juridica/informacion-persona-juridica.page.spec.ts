import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionPersonaJuridicaPage } from './informacion-persona-juridica.page';

describe('InformacionPersonaJuridicaPage', () => {
  let component: InformacionPersonaJuridicaPage;
  let fixture: ComponentFixture<InformacionPersonaJuridicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionPersonaJuridicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionPersonaJuridicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
