import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';

import { IonicModule } from '@ionic/angular';

import { InformacionPersonaJuridicaPageRoutingModule } from './informacion-persona-juridica-routing.module';

import { InformacionPersonaJuridicaPage } from './informacion-persona-juridica.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    BrMaskerModule,
    InformacionPersonaJuridicaPageRoutingModule
  ],
  declarations: [InformacionPersonaJuridicaPage]
})
export class InformacionPersonaJuridicaPageModule {}
