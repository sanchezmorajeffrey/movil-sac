import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionPersonaJuridicaPage } from './informacion-persona-juridica.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionPersonaJuridicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionPersonaJuridicaPageRoutingModule {}
