import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PersonaJuridicaService } from 'src/app/services/persona-juridica.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Paises } from '../informacion-persona/paises';
import { InformacionGeneralService } from 'src/app/services/informacion-general.service';
import { ActividadesService } from 'src/app/services/actividades.service';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-informacion-persona-juridica',
  templateUrl: './informacion-persona-juridica.page.html',
  styleUrls: ['./informacion-persona-juridica.page.scss'],
})
export class InformacionPersonaJuridicaPage implements OnInit {
  preguntas: any[] = [];
  idPresolicitud: any;
  idSeccion: any;
  tipoPersona: any;
  datosDepartamento: any[] = [];
  datosMunicipio: any[] = [];
  idSeccionContinua: any;
  selectIdMunicipio = 0;
  submitted = false;
  formPersonaJuridica: FormGroup;
  dataPaises = Paises;
  idDepartamento: any;
  selectIdDepartamento = 0;
  datosNacionalidad: any[] = [];
  datosOcupacion: any[] = [];
  selectIdPais = 0;
  selectIdNacionalidad = 0;
  selectIdOcupacion = 0;


  constructor(private router: Router, private ruta: ActivatedRoute,
              private storage: Storage,
              public personaJuridica: PersonaJuridicaService,
              public formBuilder: FormBuilder,
              private route: ActivatedRoute,
              public alertController: AlertController,
              public informacion: InformacionGeneralService,
              private actividad: ActividadesService, private navCtrl: NavController) {
    this.formPersonaJuridica = this.createMyFormPersonaJuridica();
  }

  ngOnInit() {
    this.ruta.params.subscribe(param => {
      this.idSeccion = param.idSeccion;
      this.tipoPersona = param.tipo;
      console.log('idSeccion', this.idSeccion);
    });
    this.cargarIdPreSolicitud();


  }

  private createMyFormPersonaJuridica() {
    return this.formBuilder.group({
      _NUMERO_REGISTRO: ['', Validators.required],
      _PAIS_CONSTITUCION: ['', Validators.required],
      _FECHA_CONSTITUCION: ['', Validators.required],
      _DIRECCION_SEDE: ['', Validators.required],
      _DEPARTAMENTO: ['', Validators.required],
      _MUNICIPIO: ['', Validators.required],
      _EMAIL: ['', Validators.compose([Validators.pattern('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$')])],
      _OCUPACION: ['', Validators.required],
      _FECHA_NACIMIENTO: ['', Validators.required],
      _NACIONALIDAD: ['', Validators.required],
      _DIRECCION_REPRESENTANTE: ['', Validators.required]
    });
  }

  obtenerPreguntas() {
    this.personaJuridica.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res: any) => {
      console.log('preguntas', res);
      this.preguntas = res;
      this.preguntas.forEach(element => {
        if (element.TIPO === 'Catalogo') {
          if (element.ID_CUESTIONARIO === 31) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosDepartamento = resCatalogo;
              console.log('departamento', this.datosDepartamento);
            });
          }
          if (element.ID_CUESTIONARIO === 32) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO, 'all', this.idDepartamento).subscribe((resCatalogo) => {
              this.datosMunicipio = resCatalogo;
              console.log('municipio', this.datosMunicipio);
            });
          }
          if (element.ID_CUESTIONARIO === 34) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosOcupacion = resCatalogo;
              console.log('ocupacion', this.datosOcupacion);
            });
          }
          if (element.ID_CUESTIONARIO === 36) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosNacionalidad = resCatalogo;
              console.log('nacionalidad', this.datosNacionalidad);
            });
          }
        }
      });
    });

  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }

  guardarPersonaJuridica() {
    this.submitted = true;
    if (this.formPersonaJuridica.invalid) {
      return;
    }

    const data = [
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[0].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_NUMERO_REGISTRO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[1].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_PAIS_CONSTITUCION').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[2].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_FECHA_CONSTITUCION').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[3].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_DIRECCION_SEDE').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[4].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_DEPARTAMENTO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[5].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_MUNICIPIO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[6].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_EMAIL').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[7].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_OCUPACION').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[8].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_FECHA_NACIMIENTO').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[9].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_NACIONALIDAD').value
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntas[10].ID_CUESTIONARIO,
        _REPUESTA: this.formPersonaJuridica.get('_DIRECCION_REPRESENTANTE').value
      }
    ];

    console.log('Data del formulario', data);
    this.actividad.guardarRespuestaActividad(data).subscribe((res) => {
      console.log('respuesta de guardado', res);
    });
    this.actividad.guardarSeccionTerminada(this.idPresolicitud, this.idSeccion).subscribe((resSeccionTerminada) => {
      console.log('guardado de seccion terminada', resSeccionTerminada);
    });
    console.log('id seccion dentro del metodo guardar', );
    this.actividad.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((resSaber) => {
      console.log('endpoint para saber la siguiente id', resSaber);
      this.idSeccionContinua = resSaber[0].ID_SECCION;
      switch (this.idSeccionContinua) {
        case 1:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 2:
          this.navCtrl.navigateRoot(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.navCtrl.navigateRoot(['/informacion-persona-juridica', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 4:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        case 5:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        case 6:
          this.navCtrl.navigateRoot(['/informacion-negocio-juridico', this.tipoPersona, this.idSeccionContinua]);
          break;
        default:
          break;
      }
    });
  }

  onChangeDepartamentoJuridico(event: any) {
    this.selectIdMunicipio = 0;
    this.datosMunicipio = [];
    this.idDepartamento = event.target.value;
    this.informacion.cargarCatalogo(this.preguntas[5].ID_CUESTIONARIO, 'all', this.idDepartamento).subscribe((resCatalogo) => {
      this.datosMunicipio = resCatalogo;
      console.log('municipio', this.datosMunicipio);
    });
  }

  get f() { return this.formPersonaJuridica.controls; }


  get _EMAIL() {
    return this.formPersonaJuridica.get('_EMAIL');
  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }

}
