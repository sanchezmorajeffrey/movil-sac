export const Paises = [
    {
        ID_LUGAR: 158,
        NOMBRE_LUGAR: 'Nicaragua'
    },
    {
        ID_LUGAR: 1,
        NOMBRE_LUGAR: 'Andorra'
    },
    {
        ID_LUGAR: 2,
        NOMBRE_LUGAR: 'Emiratos Árabes Unidos'
    },
    {
        ID_LUGAR: 3,
        NOMBRE_LUGAR: 'Afganistán'
    },
    {
        ID_LUGAR: 4,
        NOMBRE_LUGAR: 'Antigua y Barbuda'
    },
    {
        ID_LUGAR: 5,
        NOMBRE_LUGAR: 'Anguilla'
    },
    {
        ID_LUGAR: 6,
        NOMBRE_LUGAR: 'Albania'
    },
    {
        ID_LUGAR: 7,
        NOMBRE_LUGAR: 'Armenia'
    },
    {
        ID_LUGAR: 8,
        NOMBRE_LUGAR: 'Antillas Holandesas'
    },
    {
        ID_LUGAR: 9,
        NOMBRE_LUGAR: 'Angola'
    },
    {
        ID_LUGAR: 10,
        NOMBRE_LUGAR: 'Antárctica'
    },
    {
        ID_LUGAR: 11,
        NOMBRE_LUGAR: 'Argentina'
    },
    {
        ID_LUGAR: 12,
        NOMBRE_LUGAR: 'Samoa Americana'
    },
    {
        ID_LUGAR: 13,
        NOMBRE_LUGAR: 'Austria'
    },
    {
        ID_LUGAR: 14,
        NOMBRE_LUGAR: 'Australia'
    },
    {
        ID_LUGAR: 15,
        NOMBRE_LUGAR: 'Araba'
    },
    {
        ID_LUGAR: 16,
        NOMBRE_LUGAR: 'Azerbaidja n'
    },
    {
        ID_LUGAR: 17,
        NOMBRE_LUGAR: 'Bosnia-Herzegovina'
    },
    {
        ID_LUGAR: 18,
        NOMBRE_LUGAR: 'Barbados'
    },
    {
        ID_LUGAR: 19,
        NOMBRE_LUGAR: 'Bangladesh'
    },
    {
        ID_LUGAR: 20,
        NOMBRE_LUGAR: 'Bélgica'
    },
    {
        ID_LUGAR: 21,
        NOMBRE_LUGAR: 'Burkina Faso'
    },
    {
        ID_LUGAR: 22,
        NOMBRE_LUGAR: 'Bulgaria'
    },
    {
        ID_LUGAR: 23,
        NOMBRE_LUGAR: 'Bahréin'
    },
    {
        ID_LUGAR: 24,
        NOMBRE_LUGAR: 'Burundi'
    },
    {
        ID_LUGAR: 25,
        NOMBRE_LUGAR: 'Benín'
    },
    {
        ID_LUGAR: 26,
        NOMBRE_LUGAR: 'Bermuda'
    },
    {
        ID_LUGAR: 27,
        NOMBRE_LUGAR: 'Brunei Da russalam'
    },
    {
        ID_LUGAR: 28,
        NOMBRE_LUGAR: 'Bolivia'
    },
    {
        ID_LUGAR: 29,
        NOMBRE_LUGAR: 'Brasil'
    },
    {
        ID_LUGAR: 30,
        NOMBRE_LUGAR: 'Bahamas'
    },
    {
        ID_LUGAR: 31,
        NOMBRE_LUGAR: 'Bután'
    },
    {
        ID_LUGAR: 32,
        NOMBRE_LUGAR: 'Bouvet Island'
    },
    {
        ID_LUGAR: 33,
        NOMBRE_LUGAR: 'Botsuana'
    },
    {
        ID_LUGAR: 34,
        NOMBRE_LUGAR: 'Belarus '
    },
    {
        ID_LUGAR: 35,
        NOMBRE_LUGAR: 'Belice'
    },
    {
        ID_LUGAR: 36,
        NOMBRE_LUGAR: 'Canada'
    },
    {
        ID_LUGAR: 37,
        NOMBRE_LUGAR: 'Cocos (Keeling) Islands'
    },
    {
        ID_LUGAR: 38,
        NOMBRE_LUGAR: 'Central African Republic'
    },
    {
        ID_LUGAR: 39,
        NOMBRE_LUGAR: 'Congo'
    },
    {
        ID_LUGAR: 40,
        NOMBRE_LUGAR: 'Suiza'
    },
    {
        ID_LUGAR: 41,
        NOMBRE_LUGAR: 'Costa de Marfil (Cote D\'Ivoire)'
    },
    {
        ID_LUGAR: 42,
        NOMBRE_LUGAR: 'Islas Cook'
    },
    {
        ID_LUGAR: 43,
        NOMBRE_LUGAR: 'Chile'
    },
    {
        ID_LUGAR: 44,
        NOMBRE_LUGAR: 'Camerún'
    },
    {
        ID_LUGAR: 45,
        NOMBRE_LUGAR: 'China'
    },
    {
        ID_LUGAR: 46,
        NOMBRE_LUGAR: 'Colombia'
    },
    {
        ID_LUGAR: 47,
        NOMBRE_LUGAR: 'Costa Rica'
    },
    {
        ID_LUGAR: 48,
        NOMBRE_LUGAR: 'Former Czechoslovakia'
    },
    {
        ID_LUGAR: 49,
        NOMBRE_LUGAR: 'Cuba'
    },
    {
        ID_LUGAR: 50,
        NOMBRE_LUGAR: 'Cape Verde'
    },
    {
        ID_LUGAR: 51,
        NOMBRE_LUGAR: 'Christmas Island'
    },
    {
        ID_LUGAR: 52,
        NOMBRE_LUGAR: 'Chipre'
    },
    {
        ID_LUGAR: 53,
        NOMBRE_LUGAR: 'Republica Checa'
    },
    {
        ID_LUGAR: 54,
        NOMBRE_LUGAR: 'Alemania'
    },
    {
        ID_LUGAR: 55,
        NOMBRE_LUGAR: 'Djibouti'
    },
    {
        ID_LUGAR: 56,
        NOMBRE_LUGAR: 'Dinamarca'
    },
    {
        ID_LUGAR: 57,
        NOMBRE_LUGAR: 'Dominica'
    },
    {
        ID_LUGAR: 58,
        NOMBRE_LUGAR: 'República Dominicana'
    },
    {
        ID_LUGAR: 59,
        NOMBRE_LUGAR: 'Argelia'
    },
    {
        ID_LUGAR: 60,
        NOMBRE_LUGAR: 'Ecuador'
    },
    {
        ID_LUGAR: 61,
        NOMBRE_LUGAR: 'Estonia'
    },
    {
        ID_LUGAR: 62,
        NOMBRE_LUGAR: 'Egipto'
    },
    {
        ID_LUGAR: 63,
        NOMBRE_LUGAR: 'Western Sahara'
    },
    {
        ID_LUGAR: 64,
        NOMBRE_LUGAR: 'Eritrea'
    },
    {
        ID_LUGAR: 65,
        NOMBRE_LUGAR: 'España'
    },
    {
        ID_LUGAR: 66,
        NOMBRE_LUGAR: 'Etiopía'
    },
    {
        ID_LUGAR: 67,
        NOMBRE_LUGAR: 'Finlandia'
    },
    {
        ID_LUGAR: 68,
        NOMBRE_LUGAR: 'Fiji'
    },
    {
        ID_LUGAR: 69,
        NOMBRE_LUGAR: 'Falkland Islands'
    },
    {
        ID_LUGAR: 70,
        NOMBRE_LUGAR: 'Micronesia'
    },
    {
        ID_LUGAR: 71,
        NOMBRE_LUGAR: 'Faroe Islands'
    },
    {
        ID_LUGAR: 72,
        NOMBRE_LUGAR: 'Francia'
    },
    {
        ID_LUGAR: 73,
        NOMBRE_LUGAR: 'Gabon'
    },
    {
        ID_LUGAR: 74,
        NOMBRE_LUGAR: 'Gran Bretaña'
    },
    {
        ID_LUGAR: 75,
        NOMBRE_LUGAR: 'Grenada'
    },
    {
        ID_LUGAR: 76,
        NOMBRE_LUGAR: 'Georgia'
    },
    {
        ID_LUGAR: 77,
        NOMBRE_LUGAR: 'Guyana Francesa'
    },
    {
        ID_LUGAR: 78,
        NOMBRE_LUGAR: 'Ghana'
    },
    {
        ID_LUGAR: 79,
        NOMBRE_LUGAR: 'Gibraltar'
    },
    {
        ID_LUGAR: 80,
        NOMBRE_LUGAR: 'Groenlandia'
    },
    {
        ID_LUGAR: 81,
        NOMBRE_LUGAR: 'Gambia '
    },
    {
        ID_LUGAR: 82,
        NOMBRE_LUGAR: 'Guinea'
    },
    {
        ID_LUGAR: 83,
        NOMBRE_LUGAR: 'Guadeloupe (French)'
    },
    {
        ID_LUGAR: 84,
        NOMBRE_LUGAR: 'Guinea Ecuatorial'
    },
    {
        ID_LUGAR: 85,
        NOMBRE_LUGAR: 'Grecia'
    },
    {
        ID_LUGAR: 86,
        NOMBRE_LUGAR: 'S. Georgia & S. Sandwich ISIS.'
    },
    {
        ID_LUGAR: 87,
        NOMBRE_LUGAR: 'Guatemala'
    },
    {
        ID_LUGAR: 88,
        NOMBRE_LUGAR: 'Guam (USA)'
    },
    {
        ID_LUGAR: 89,
        NOMBRE_LUGAR: 'Guinea Bissau'
    },
    {
        ID_LUGAR: 90,
        NOMBRE_LUGAR: 'Guyana'
    },
    {
        ID_LUGAR: 91,
        NOMBRE_LUGAR: 'Hong Kong'
    },
    {
        ID_LUGAR: 92,
        NOMBRE_LUGAR: 'Heard and McDonald Islands'
    },
    {
        ID_LUGAR: 93,
        NOMBRE_LUGAR: 'Honduras'
    },
    {
        ID_LUGAR: 94,
        NOMBRE_LUGAR: 'Croacia'
    },
    {
        ID_LUGAR: 95,
        NOMBRE_LUGAR: 'Haiti'
    },
    {
        ID_LUGAR: 96,
        NOMBRE_LUGAR: 'Hungría'
    },
    {
        ID_LUGAR: 97,
        NOMBRE_LUGAR: 'Indonesia'
    },
    {
        ID_LUGAR: 98,
        NOMBRE_LUGAR: 'Irlanda'
    },
    {
        ID_LUGAR: 99,
        NOMBRE_LUGAR: 'Israel'
    },
    {
        ID_LUGAR: 100,
        NOMBRE_LUGAR: 'India'
    },
    {
        ID_LUGAR: 101,
        NOMBRE_LUGAR: 'British Indian Ocean Territory'
    },
    {
        ID_LUGAR: 102,
        NOMBRE_LUGAR: 'Irak'
    },
    {
        ID_LUGAR: 103,
        NOMBRE_LUGAR: 'Irán'
    },
    {
        ID_LUGAR: 104,
        NOMBRE_LUGAR: 'Islandia'
    },
    {
        ID_LUGAR: 105,
        NOMBRE_LUGAR: 'Italia'
    },
    {
        ID_LUGAR: 106,
        NOMBRE_LUGAR: 'Jamaica'
    },
    {
        ID_LUGAR: 107,
        NOMBRE_LUGAR: 'Jordania'
    },
    {
        ID_LUGAR: 108,
        NOMBRE_LUGAR: 'Japón'
    },
    {
        ID_LUGAR: 109,
        NOMBRE_LUGAR: 'Kenia'
    },
    {
        ID_LUGAR: 110,
        NOMBRE_LUGAR: 'Kirguizistán'
    },
    {
        ID_LUGAR: 111,
        NOMBRE_LUGAR: 'Cambodia'
    },
    {
        ID_LUGAR: 112,
        NOMBRE_LUGAR: 'Kiribati'
    },
    {
        ID_LUGAR: 113,
        NOMBRE_LUGAR: 'Comoros'
    },
    {
        ID_LUGAR: 114,
        NOMBRE_LUGAR: 'Saint Kitts & Nevis Anguilla'
    },
    {
        ID_LUGAR: 115,
        NOMBRE_LUGAR: 'Corea del Norte'
    },
    {
        ID_LUGAR: 116,
        NOMBRE_LUGAR: 'Corea del Sur'
    },
    {
        ID_LUGAR: 117,
        NOMBRE_LUGAR: 'Kuwait'
    },
    {
        ID_LUGAR: 118,
        NOMBRE_LUGAR: 'Islas Caimán'
    },
    {
        ID_LUGAR: 119,
        NOMBRE_LUGAR: 'Kazakstán'
    },
    {
        ID_LUGAR: 120,
        NOMBRE_LUGAR: 'Laos'
    },
    {
        ID_LUGAR: 121,
        NOMBRE_LUGAR: 'Lebanon'
    },
    {
        ID_LUGAR: 122,
        NOMBRE_LUGAR: 'Saint Lucia'
    },
    {
        ID_LUGAR: 123,
        NOMBRE_LUGAR: 'Liechtenstein'
    },
    {
        ID_LUGAR: 124,
        NOMBRE_LUGAR: 'Sri Lanka'
    },
    {
        ID_LUGAR: 125,
        NOMBRE_LUGAR: 'Liberia'
    },
    {
        ID_LUGAR: 126,
        NOMBRE_LUGAR: 'Lesoto'
    },
    {
        ID_LUGAR: 127,
        NOMBRE_LUGAR: 'Lituania'
    },
    {
        ID_LUGAR: 128,
        NOMBRE_LUGAR: 'Luxemburgo'
    },
    {
        ID_LUGAR: 129,
        NOMBRE_LUGAR: 'Latvia '
    },
    {
        ID_LUGAR: 130,
        NOMBRE_LUGAR: 'Libia'
    },
    {
        ID_LUGAR: 131,
        NOMBRE_LUGAR: 'Moruecos'
    },
    {
        ID_LUGAR: 132,
        NOMBRE_LUGAR: 'Monaco'
    },
    {
        ID_LUGAR: 133,
        NOMBRE_LUGAR: 'Moldavia'
    },
    {
        ID_LUGAR: 134,
        NOMBRE_LUGAR: 'Madagascar'
    },
    {
        ID_LUGAR: 135,
        NOMBRE_LUGAR: 'Marshall Islands'
    },
    {
        ID_LUGAR: 136,
        NOMBRE_LUGAR: 'USA Military'
    },
    {
        ID_LUGAR: 137,
        NOMBRE_LUGAR: 'Macedonia'
    },
    {
        ID_LUGAR: 138,
        NOMBRE_LUGAR: 'Mali'
    },
    {
        ID_LUGAR: 139,
        NOMBRE_LUGAR: 'Myanmar'
    },
    {
        ID_LUGAR: 140,
        NOMBRE_LUGAR: 'Mongolia'
    },
    {
        ID_LUGAR: 141,
        NOMBRE_LUGAR: 'Macau'
    },
    {
        ID_LUGAR: 142,
        NOMBRE_LUGAR: 'Northern Mariana Islands'
    },
    {
        ID_LUGAR: 143,
        NOMBRE_LUGAR: 'Martinique (French)'
    },
    {
        ID_LUGAR: 144,
        NOMBRE_LUGAR: 'Mauritania'
    },
    {
        ID_LUGAR: 145,
        NOMBRE_LUGAR: 'Montserrat'
    },
    {
        ID_LUGAR: 146,
        NOMBRE_LUGAR: 'Malta'
    },
    {
        ID_LUGAR: 147,
        NOMBRE_LUGAR: 'Mauritius'
    },
    {
        ID_LUGAR: 148,
        NOMBRE_LUGAR: 'Mal dives'
    },
    {
        ID_LUGAR: 149,
        NOMBRE_LUGAR: 'Malawi'
    },
    {
        ID_LUGAR: 150,
        NOMBRE_LUGAR: 'México'
    },
    {
        ID_LUGAR: 151,
        NOMBRE_LUGAR: 'Malaysia'
    },
    {
        ID_LUGAR: 152,
        NOMBRE_LUGAR: 'Mozambique'
    },
    {
        ID_LUGAR: 153,
        NOMBRE_LUGAR: 'Namibia'
    },
    {
        ID_LUGAR: 154,
        NOMBRE_LUGAR: 'New Caledonia (French)'
    },
    {
        ID_LUGAR: 155,
        NOMBRE_LUGAR: 'Níger'
    },
    {
        ID_LUGAR: 156,
        NOMBRE_LUGAR: 'Norfolk Island'
    },
    {
        ID_LUGAR: 157,
        NOMBRE_LUGAR: 'Nigeria'
    },
    {
        ID_LUGAR: 159,
        NOMBRE_LUGAR: 'Holanda'
    },
    {
        ID_LUGAR: 160,
        NOMBRE_LUGAR: 'Noruega'
    },
    {
        ID_LUGAR: 161,
        NOMBRE_LUGAR: 'Nepal'
    },
    {
        ID_LUGAR: 162,
        NOMBRE_LUGAR: 'Nauru'
    },
    {
        ID_LUGAR: 163,
        NOMBRE_LUGAR: 'Niue'
    },
    {
        ID_LUGAR: 164,
        NOMBRE_LUGAR: 'Nueva Zelanda'
    },
    {
        ID_LUGAR: 165,
        NOMBRE_LUGAR: 'Oman'
    },
    {
        ID_LUGAR: 166,
        NOMBRE_LUGAR: 'Panamá'
    },
    {
        ID_LUGAR: 167,
        NOMBRE_LUGAR: 'perú'
    },
    {
        ID_LUGAR: 168,
        NOMBRE_LUGAR: 'Polynesia (French)'
    },
    {
        ID_LUGAR: 169,
        NOMBRE_LUGAR: 'Papua New Guinea'
    },
    {
        ID_LUGAR: 170,
        NOMBRE_LUGAR: 'Filipinas'
    },
    {
        ID_LUGAR: 171,
        NOMBRE_LUGAR: 'Pakistán'
    },
    {
        ID_LUGAR: 172,
        NOMBRE_LUGAR: 'Polonia'
    },
    {
        ID_LUGAR: 173,
        NOMBRE_LUGAR: 'Saint Pierre and Miquelon'
    },
    {
        ID_LUGAR: 174,
        NOMBRE_LUGAR: 'Pitcairn Island'
    },
    {
        ID_LUGAR: 175,
        NOMBRE_LUGAR: 'Puerto Rico'
    },
    {
        ID_LUGAR: 176,
        NOMBRE_LUGAR: 'Portugal'
    },
    {
        ID_LUGAR: 177,
        NOMBRE_LUGAR: 'Palau'
    },
    {
        ID_LUGAR: 178,
        NOMBRE_LUGAR: 'Paraguay'
    },
    {
        ID_LUGAR: 179,
        NOMBRE_LUGAR: 'Qatar'
    },
    {
        ID_LUGAR: 180,
        NOMBRE_LUGAR: 'Reunion (French)'
    },
    {
        ID_LUGAR: 181,
        NOMBRE_LUGAR: 'Rumania'
    },
    {
        ID_LUGAR: 182,
        NOMBRE_LUGAR: 'Rusia'
    },
    {
        ID_LUGAR: 183,
        NOMBRE_LUGAR: 'Rwanda'
    },
    {
        ID_LUGAR: 184,
        NOMBRE_LUGAR: 'Arabia Saudita'
    },
    {
        ID_LUGAR: 185,
        NOMBRE_LUGAR: 'Solomon Islands'
    },
    {
        ID_LUGAR: 186,
        NOMBRE_LUGAR: 'Seychelles'
    },
    {
        ID_LUGAR: 187,
        NOMBRE_LUGAR: 'Sudan'
    },
    {
        ID_LUGAR: 188,
        NOMBRE_LUGAR: 'Suecia'
    },
    {
        ID_LUGAR: 189,
        NOMBRE_LUGAR: 'Singapore'
    },
    {
        ID_LUGAR: 190,
        NOMBRE_LUGAR: 'Saint Helena'
    },
    {
        ID_LUGAR: 191,
        NOMBRE_LUGAR: 'Slovenia'
    },
    {
        ID_LUGAR: 192,
        NOMBRE_LUGAR: 'Svalbard and Jan Mayen Islands'
    },
    {
        ID_LUGAR: 193,
        NOMBRE_LUGAR: 'Republica Eslovaquia'
    },
    {
        ID_LUGAR: 194,
        NOMBRE_LUGAR: 'Sierra Leone'
    },
    {
        ID_LUGAR: 195,
        NOMBRE_LUGAR: 'San Marino'
    },
    {
        ID_LUGAR: 196,
        NOMBRE_LUGAR: 'Senegal'
    },
    {
        ID_LUGAR: 197,
        NOMBRE_LUGAR: 'Somalia'
    },
    {
        ID_LUGAR: 198,
        NOMBRE_LUGAR: 'Suriname'
    },
    {
        ID_LUGAR: 199,
        NOMBRE_LUGAR: 'Saint Tome (Sao Tome) and Principe'
    },
    {
        ID_LUGAR: 200,
        NOMBRE_LUGAR: 'Former USSR'
    },
    {
        ID_LUGAR: 201,
        NOMBRE_LUGAR: 'El Salvador'
    },
    {
        ID_LUGAR: 202,
        NOMBRE_LUGAR: 'Siria'
    },
    {
        ID_LUGAR: 203,
        NOMBRE_LUGAR: 'Swaziland'
    },
    {
        ID_LUGAR: 204,
        NOMBRE_LUGAR: 'Turks and Caicos Islands'
    },
    {
        ID_LUGAR: 205,
        NOMBRE_LUGAR: 'Chad'
    },
    {
        ID_LUGAR: 206,
        NOMBRE_LUGAR: 'French Southern Territorios'
    },
    {
        ID_LUGAR: 207,
        NOMBRE_LUGAR: 'Togo'
    },
    {
        ID_LUGAR: 208,
        NOMBRE_LUGAR: 'Tailandia'
    },
    {
        ID_LUGAR: 209,
        NOMBRE_LUGAR: 'Tadjikistan'
    },
    {
        ID_LUGAR: 210,
        NOMBRE_LUGAR: 'Tokelau'
    },
    {
        ID_LUGAR: 211,
        NOMBRE_LUGAR: 'Turkmenistán'
    },
    {
        ID_LUGAR: 212,
        NOMBRE_LUGAR: 'Tunisia'
    },
    {
        ID_LUGAR: 213,
        NOMBRE_LUGAR: 'Tonga'
    },
    {
        ID_LUGAR: 214,
        NOMBRE_LUGAR: 'East Timor'
    },
    {
        ID_LUGAR: 215,
        NOMBRE_LUGAR: 'Turquía'
    },
    {
        ID_LUGAR: 216,
        NOMBRE_LUGAR: 'Trinidad y Tobago'
    },
    {
        ID_LUGAR: 217,
        NOMBRE_LUGAR: 'Tuvalu'
    },
    {
        ID_LUGAR: 218,
        NOMBRE_LUGAR: 'Taiwán'
    },
    {
        ID_LUGAR: 219,
        NOMBRE_LUGAR: 'Tanzania'
    },
    {
        ID_LUGAR: 220,
        NOMBRE_LUGAR: 'Ucraine'
    },
    {
        ID_LUGAR: 221,
        NOMBRE_LUGAR: 'Uganda'
    },
    {
        ID_LUGAR: 222,
        NOMBRE_LUGAR: 'Reino Unido'
    },
    {
        ID_LUGAR: 223,
        NOMBRE_LUGAR: 'USA Minor Outlying Islands'
    },
    {
        ID_LUGAR: 224,
        NOMBRE_LUGAR: 'Estados Unidos'
    },
    {
        ID_LUGAR: 225,
        NOMBRE_LUGAR: 'Uruguay'
    },
    {
        ID_LUGAR: 226,
        NOMBRE_LUGAR: 'Uzbekistán'
    },
    {
        ID_LUGAR: 227,
        NOMBRE_LUGAR: 'Ciudad de Vaticano'
    },
    {
        ID_LUGAR: 228,
        NOMBRE_LUGAR: 'San Vincente & Granadinas'
    },
    {
        ID_LUGAR: 229,
        NOMBRE_LUGAR: 'Venezuela'
    },
    {
        ID_LUGAR: 230,
        NOMBRE_LUGAR: 'Virgin Islands (British)'
    },
    {
        ID_LUGAR: 231,
        NOMBRE_LUGAR: 'Virgin Islands (USA)'
    },
    {
        ID_LUGAR: 232,
        NOMBRE_LUGAR: 'Vietnam'
    },
    {
        ID_LUGAR: 233,
        NOMBRE_LUGAR: 'Vanuatu'
    },
    {
        ID_LUGAR: 234,
        NOMBRE_LUGAR: 'Wallis and Futuna Islands'
    },
    {
        ID_LUGAR: 235,
        NOMBRE_LUGAR: 'Samoa'
    },
    {
        ID_LUGAR: 236,
        NOMBRE_LUGAR: 'Yemen'
    },
    {
        ID_LUGAR: 237,
        NOMBRE_LUGAR: 'Mayotte'
    },
    {
        ID_LUGAR: 238,
        NOMBRE_LUGAR: 'Yugoslavia'
    },
    {
        ID_LUGAR: 239,
        NOMBRE_LUGAR: 'Suráfrica'
    },
    {
        ID_LUGAR: 240,
        NOMBRE_LUGAR: 'Zambia'
    },
    {
        ID_LUGAR: 241,
        NOMBRE_LUGAR: 'Zaire'
    },
    {
        ID_LUGAR: 242,
        NOMBRE_LUGAR: 'Zimbawe'
    }
];



