import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformacionNegocioNaturalPage } from './informacion-negocio-natural.page';

describe('InformacionNegocioNaturalPage', () => {
  let component: InformacionNegocioNaturalPage;
  let fixture: ComponentFixture<InformacionNegocioNaturalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionNegocioNaturalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformacionNegocioNaturalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
