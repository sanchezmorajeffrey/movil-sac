import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacionNegocioNaturalPageRoutingModule } from './informacion-negocio-natural-routing.module';

import { InformacionNegocioNaturalPage } from './informacion-negocio-natural.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    InformacionNegocioNaturalPageRoutingModule
  ],
  declarations: [InformacionNegocioNaturalPage]
})
export class InformacionNegocioNaturalPageModule {}
