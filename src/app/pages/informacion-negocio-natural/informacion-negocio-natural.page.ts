import { Component, OnInit } from '@angular/core';
import { ActividadesService } from 'src/app/services/actividades.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaJuridicaService } from 'src/app/services/persona-juridica.service';

import { Paises } from '../informacion-persona/paises';
import { InformacionGeneralService } from 'src/app/services/informacion-general.service';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-informacion-negocio-natural',
  templateUrl: './informacion-negocio-natural.page.html',
  styleUrls: ['./informacion-negocio-natural.page.scss'],
})
export class InformacionNegocioNaturalPage implements OnInit {
  formInformacionNegocio: FormGroup;
  idSeccionContinua: any;
  idSeccion: any;
  tipoPersona: any;
  idPresolicitud: any;
  preguntas: any[] = [];
  datosEntidadImpuesto: any[] = [];
  prueba: any;
  selectedLanguage: any;
  submitted = false;
  comoEmpezoNegocio: any = [];
  esAlquilado = false;
  tiempoPermanencia = false;
  datosDepartamento: any[] = [];
  datosMunicipio: any[] = [];
  idDepartamento: any;
  selectIdMunicipio = 0;
  mostrar = false;


  negocioPropio = false;
  constructor(private router: Router, private ruta: ActivatedRoute,
              private storage: Storage,
              public personaJuridica: PersonaJuridicaService,
              public formBuilder: FormBuilder,
              private route: ActivatedRoute,
              public alertController: AlertController,
              public informacion: InformacionGeneralService,
              private actividad: ActividadesService, private navCtrl: NavController) {
                this.formInformacionNegocio = this.createMyFormInformacionNegocio();
              }

  ngOnInit() {
    this.ruta.params.subscribe(param => {
      this.idSeccion = param.idSeccion;
      this.tipoPersona = param.tipo;
      console.log('idSeccion', this.idSeccion);
      console.log('tipoPersoona', this.tipoPersona);
    });
    this.cargarIdPreSolicitud();
  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }

  obtenerPreguntas() {
    this.personaJuridica.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res: any) => {
      console.log('preguntas', res);
      this.preguntas = res;

      this.preguntas.forEach(element => {
        if (element.TIPO === 'Catalogo') {
          if (element.ID_CUESTIONARIO === 68) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.comoEmpezoNegocio = resCatalogo;
              console.log('comoInicioElnecogio', this.comoEmpezoNegocio);
            });
          }
          if (element.ID_CUESTIONARIO === 73) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosDepartamento = resCatalogo;
              console.log('Departamento', this.datosDepartamento);
            });
          }
          if (element.ID_CUESTIONARIO === 74) {
            this.informacion.cargarCatalogo(element.ID_CUESTIONARIO).subscribe((resCatalogo) => {
              this.datosMunicipio = resCatalogo;
              console.log('Municipio', this.datosMunicipio);
            });
          }
        }
      });
    });

  }


  private createMyFormInformacionNegocio() {
    return this.formBuilder.group({
      _TIEMPO_EXISTENCIA: ['', Validators.required],
      _COMO_EMPEZO_NEGOCIO: ['', Validators.required],
      _VIVIENDA_NEGOCIO_ALQUILADO: [''],
      _LOCAL_PROPIO: [''],
      _TIEMPO_PERMANENCIA_ALQUILER: [''],
      _DEPARTAMENTO: [''],
      _MUNICIPIO: [''],
      _DIRECCION_EXACTA: ['']
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }


  guardarInformacionNegocio() {

  }

  changeNegocioEsLocalAlquilado(event: any) {
    console.log('la vivienda y el negocio?', event.target.value);
    const local = event.target.value;
    if (local === 'No') {
      this.esAlquilado = true;
      this.mostrar = false;
      // this.tiempoPermanencia = true;
    } else {
      this.esAlquilado = false;
    }
  }

  changeTiempoPermanencia(event: any) {
    console.log('el local del negocio es alquilado?', event.target.value);
    const local = event.target.value;
    if (local === 'Sí') {
      this.tiempoPermanencia = true;
    } else {
      this.tiempoPermanencia = false;
    }
  }


  onChangeDepartamentoJuridico(event: any) {
    this.selectIdMunicipio = 0;
    this.datosMunicipio = [];
    this.idDepartamento = event.target.value;
    this.informacion.cargarCatalogo(this.preguntas[7].ID_CUESTIONARIO, 'all', this.idDepartamento).subscribe((resCatalogo) => {
      this.datosMunicipio = resCatalogo;
      console.log('municipio', this.datosMunicipio);
    });
  }


  get f() { return this.formInformacionNegocio.controls; }

}
