import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionNegocioNaturalPage } from './informacion-negocio-natural.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionNegocioNaturalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionNegocioNaturalPageRoutingModule {}
