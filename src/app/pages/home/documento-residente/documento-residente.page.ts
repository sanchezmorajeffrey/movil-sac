import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PreClienteService } from 'src/app/services/pre-cliente.service';
import { NavController, } from '@ionic/angular';


@Component({
  selector: 'app-documento-residente',
  templateUrl: './documento-residente.page.html',
  styleUrls: ['./documento-residente.page.scss'],
})
export class DocumentoResidentePage implements OnInit {
  formDocumentoResidente: FormGroup;
  submitted = false;
  activo = false;
  fechaNacimiento: any;
  edad: any = 0;
  documento: any;
  datosCliente: any[] = [];
  constructor(private preCliente: PreClienteService, public formBuilder: FormBuilder, private router: Router,
              private navCtrl: NavController) {
    this.formDocumentoResidente = this.formDocumentoNutural();
  }

  ngOnInit() {
  }

  private formDocumentoNutural() {
    return this.formBuilder.group({
      _IDENTIDAD: ['', Validators.required],
      _PRIMER_NOMBRE: [{ value: '', disabled: false }, Validators.required],
      _SEGUNDO_NOMBRE: [{ value: '', disabled: false }],
      _PRIMER_APELLIDO: [{ value: '', disabled: false }, Validators.required],
      _SEGUNDO_APELLIDO: [{ value: '', disabled: false }],
      _MOVIL: [{ value: '', disabled: false }, Validators.required],
      _TELEFONO: [''],
      _GENERO: [{ value: '', disabled: false }, Validators.required],
      _CODIGO_ESTADO_CIVIL: [''],
      _NOMBRE_COMERCIAL: [''],
      _NOMBRE_CONTACTO: [''],
      _TELEFONO_CONTACTO: [''],
      _TIPO_CLIENTE: ['NE'],
      _DIRECCION: [''],
      _FECHA_NACIMIENTO: [{ value: '', disabled: false }, Validators.required]
    });
  }


  registrarPreCliente() {
    this.submitted = true;
    if (this.formDocumentoResidente.invalid) {
      return;
    }
    this.documento = this.formDocumentoResidente.get('_IDENTIDAD').value;
    const data = this.formDocumentoResidente.getRawValue();
    this.preCliente.registrarPreCliente(data).subscribe((res) => {
      console.log('mira aqui!', res);
      this.navCtrl.navigateRoot(['/solicitud', res._id_pre_precliente, this.documento, 'NE']);
      this.formDocumentoResidente.reset();
    });
    this.submitted = false;
  }


  get _IDENTIDAD() {
    return this.formDocumentoResidente.get('_IDENTIDAD');
  }


  get f() { return this.formDocumentoResidente.controls; }


  validateUserIDNotTaken(control: any) {
    return this.preCliente.buscarClienteCedula(control).subscribe((res) => {
      this.datosCliente = res;
      console.log('res', this.datosCliente);
      if (this.datosCliente.length > 0) {
        console.log('hay coincidencia');
        this.formDocumentoResidente.get('_PRIMER_NOMBRE').setValue(this.datosCliente[0].PRIMER_NOMBRE);
        this.formDocumentoResidente.get('_PRIMER_NOMBRE').disable();
        this.formDocumentoResidente.get('_SEGUNDO_NOMBRE').setValue(this.datosCliente[0].SEGUNDO_NOMBRE);
        this.formDocumentoResidente.get('_SEGUNDO_NOMBRE').disable();
        this.formDocumentoResidente.get('_PRIMER_APELLIDO').setValue(this.datosCliente[0].PRIMER_APELLIDO);
        this.formDocumentoResidente.get('_PRIMER_APELLIDO').disable();
        this.formDocumentoResidente.get('_SEGUNDO_APELLIDO').setValue(this.datosCliente[0].SEGUNDO_APELLIDO);
        this.formDocumentoResidente.get('_SEGUNDO_APELLIDO').disable();
        this.formDocumentoResidente.get('_TELEFONO').setValue(this.datosCliente[0].TELEFONO);
        this.formDocumentoResidente.get('_MOVIL').setValue(this.datosCliente[0].MOVIL);
        this.formDocumentoResidente.get('_GENERO').setValue(this.datosCliente[0].GENERO);
        this.formDocumentoResidente.get('_FECHA_NACIMIENTO').setValue(this.datosCliente[0].FECHA_NACIMIENTO);
        this.formDocumentoResidente.get('_FECHA_NACIMIENTO').disable();
        this.activo = true;
      } else {
        console.log('no hay coincidencia');
        this.formDocumentoResidente.get('_PRIMER_NOMBRE').setValue('');
        this.formDocumentoResidente.get('_SEGUNDO_NOMBRE').setValue('');
        this.formDocumentoResidente.get('_PRIMER_APELLIDO').setValue('');
        this.formDocumentoResidente.get('_SEGUNDO_APELLIDO').setValue('');
        this.formDocumentoResidente.get('_TELEFONO').setValue('');
        this.formDocumentoResidente.get('_MOVIL').setValue('');
        this.formDocumentoResidente.get('_GENERO').setValue('');
        this.formDocumentoResidente.get('_FECHA_NACIMIENTO').setValue('');
        this.formDocumentoResidente.get('_PRIMER_NOMBRE').enable();
        this.formDocumentoResidente.get('_SEGUNDO_NOMBRE').enable();
        this.formDocumentoResidente.get('_PRIMER_APELLIDO').enable();
        this.formDocumentoResidente.get('_SEGUNDO_APELLIDO').enable();
        this.formDocumentoResidente.get('_TELEFONO').enable();
        this.formDocumentoResidente.get('_MOVIL').enable();
        this.formDocumentoResidente.get('_GENERO').enable();
        this.formDocumentoResidente.get('_FECHA_NACIMIENTO').enable();
        this.activo = false;
      }
    });
  }


  cedulaCliente(event: any) {
    event.preventDefault();
    this.validateUserIDNotTaken(event.target.value);
  }

}
