import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { DocumentoResidentePageRoutingModule } from './documento-residente-routing.module';

import { DocumentoResidentePage } from './documento-residente.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BrMaskerModule,
    DocumentoResidentePageRoutingModule
  ],
  declarations: [DocumentoResidentePage]
})
export class DocumentoResidentePageModule {}
