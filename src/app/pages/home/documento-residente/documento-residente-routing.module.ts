import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentoResidentePage } from './documento-residente.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentoResidentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentoResidentePageRoutingModule {}
