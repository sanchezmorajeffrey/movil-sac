import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DocumentoResidentePage } from './documento-residente.page';

describe('DocumentoResidentePage', () => {
  let component: DocumentoResidentePage;
  let fixture: ComponentFixture<DocumentoResidentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoResidentePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DocumentoResidentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
