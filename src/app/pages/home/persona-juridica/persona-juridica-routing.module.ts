import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonaJuridicaPage } from './persona-juridica.page';

const routes: Routes = [
  {
    path: '',
    component: PersonaJuridicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonaJuridicaPageRoutingModule {}
