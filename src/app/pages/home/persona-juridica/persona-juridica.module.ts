import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonaJuridicaPageRoutingModule } from './persona-juridica-routing.module';

import { PersonaJuridicaPage } from './persona-juridica.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrMaskerModule,
    ReactiveFormsModule,
    PersonaJuridicaPageRoutingModule
  ],
  declarations: [PersonaJuridicaPage]
})
export class PersonaJuridicaPageModule {}
