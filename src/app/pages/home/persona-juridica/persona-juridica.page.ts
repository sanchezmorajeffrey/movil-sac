import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PreClienteService } from 'src/app/services/pre-cliente.service';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-persona-juridica',
  templateUrl: './persona-juridica.page.html',
  styleUrls: ['./persona-juridica.page.scss'],
})
export class PersonaJuridicaPage implements OnInit {
  formDocumentoRUC: FormGroup;
  submitted = false;
  documento: any;
  datosCliente: any[] = [];
  activo = false;
  contador = '';
  mascara = { mask: '000-000000-00000', len: 16, userCaracters: true };

  constructor(private preCliente: PreClienteService, public formBuilder: FormBuilder, private router: Router,
              private navCtrl: NavController) {
    this.formDocumentoRUC = this.formDocumentoJuridico();
  }

  ngOnInit() {
  }


  get inputRUC() {
    return this.formDocumentoRUC.get('inputRUC');
  }



  private formDocumentoJuridico() {
    return this.formBuilder.group({
      _IDENTIDAD: ['', Validators.compose([Validators.required,
      Validators.maxLength(18)])],
      _PRIMER_NOMBRE: [''],
      _SEGUNDO_NOMBRE: [''],
      _PRIMER_APELLIDO: [''],
      _SEGUNDO_APELLIDO: [''],
      _MOVIL: [''],
      _TELEFONO: ['', Validators.required],
      _GENERO: [''],
      _CODIGO_ESTADO_CIVIL: [''],
      _NOMBRE_COMERCIAL: [{ value: '', disabled: false }, Validators.required],
      _NOMBRE_CONTACTO: ['', Validators.required],
      _RAZON_SOCIAL: [{ value: '', disabled: false }, Validators.required],
      _TELEFONO_CONTACTO: ['', Validators.required],
      _TIPO_CLIENTE: ['J'],
      _DIRECCION: [''],
      _FECHA_NACIMIENTO: [''],
      _ID_REPRESENTANTE: ['', Validators.required],
    });
  }




  consultarDocumentoNatural() {
    this.submitted = true;
    if (this.formDocumentoRUC.invalid) {
      return;
    }
    this.documento = this.formDocumentoRUC.get('_IDENTIDAD').value;
    const data = this.formDocumentoRUC.getRawValue();
    this.preCliente.registrarPreCliente(data).subscribe((res) => {
      console.log('mira aqui!', res);
      // this.router.navigate(['/solicitud', res._id_pre_precliente, this.documento, 'J']);
      this.navCtrl.navigateRoot(['/solicitud', res._id_pre_precliente, this.documento, 'J']);
      // this.formDocumentoRUC.reset();
    });
    this.submitted = false;
  }


  get _IDENTIDAD() {
    return this.formDocumentoRUC.get('_IDENTIDAD');
  }


  get f() { return this.formDocumentoRUC.controls; }


  validateUserIDNotTaken(control: any) {
    return this.preCliente.buscarClienteCedula(control).subscribe((res) => {
      this.datosCliente = res;
      console.log('res', this.datosCliente);
      if (this.datosCliente.length > 0) {
        console.log('hay coincidencia');
        this.formDocumentoRUC.get('_PRIMER_NOMBRE').setValue(this.datosCliente[0].PRIMER_NOMBRE);
        this.formDocumentoRUC.get('_SEGUNDO_NOMBRE').setValue(this.datosCliente[0].SEGUNDO_NOMBRE);
        this.formDocumentoRUC.get('_PRIMER_APELLIDO').setValue(this.datosCliente[0].PRIMER_APELLIDO);
        this.formDocumentoRUC.get('_SEGUNDO_APELLIDO').setValue(this.datosCliente[0].SEGUNDO_APELLIDO);
        this.formDocumentoRUC.get('_TELEFONO').setValue(this.datosCliente[0].TELEFONO);
        this.formDocumentoRUC.get('_MOVIL').setValue(this.datosCliente[0].MOVIL);
        this.formDocumentoRUC.get('_GENERO').setValue(this.datosCliente[0].GENERO);
        this.formDocumentoRUC.get('_FECHA_NACIMIENTO').setValue(this.datosCliente[0].FECHA_NACIMIENTO);
        this.formDocumentoRUC.get('_NOMBRE_COMERCIAL').setValue(this.datosCliente[0].NOMBRE_COMERCIAL);
        this.formDocumentoRUC.get('_NOMBRE_COMERCIAL').disable();
        this.formDocumentoRUC.get('_RAZON_SOCIAL').setValue(this.datosCliente[0].RAZON_SOCIAL);
        this.formDocumentoRUC.get('_ID_REPRESENTANTE').setValue(this.datosCliente[0].ID_REPRESENTANTE);
        this.formDocumentoRUC.get('_TELEFONO_CONTACTO').setValue(this.datosCliente[0].TELEFONO_CONTACTO);
        this.formDocumentoRUC.get('_NOMBRE_CONTACTO').setValue(this.datosCliente[0].NOMBRE_CONTACTO);
        this.formDocumentoRUC.get('_RAZON_SOCIAL').setValue(this.datosCliente[0].RAZON_SOCIAL);
        this.formDocumentoRUC.get('RAZON_SOCIAL').disable();
        this.activo = true;

      } else {
        console.log('no hay coincidencia');
        this.formDocumentoRUC.get('_PRIMER_NOMBRE').setValue('');
        this.formDocumentoRUC.get('_SEGUNDO_NOMBRE').setValue('');
        this.formDocumentoRUC.get('_PRIMER_APELLIDO').setValue('');
        this.formDocumentoRUC.get('_SEGUNDO_APELLIDO').setValue('');
        this.formDocumentoRUC.get('_TELEFONO').setValue('');
        this.formDocumentoRUC.get('_MOVIL').setValue('');
        this.formDocumentoRUC.get('_ID_REPRESENTANTE').setValue('');
        this.formDocumentoRUC.get('_GENERO').setValue('');
        this.formDocumentoRUC.get('_FECHA_NACIMIENTO').setValue('');
        this.formDocumentoRUC.get('_NOMBRE_COMERCIAL').setValue('');
        this.formDocumentoRUC.get('_NOMBRE_COMERCIAL').enable();
        this.formDocumentoRUC.get('_RAZON_SOCIAL').setValue('');
        this.formDocumentoRUC.get('_TELEFONO_CONTACTO').setValue('');
        this.formDocumentoRUC.get('_NOMBRE_CONTACTO').setValue('');
        this.activo = false;
      }
    });
  }



  cedulaRUC(event: any) {
    event.preventDefault();
    this.validateUserIDNotTaken(event.target.value);
  }

  cedulaRepresentante(event: any) {
    event.preventDefault();
    console.log('event', event.target.value);
    this.contador = event.target.value;
    this.contador = this.contador.toString();
    if (this.contador.length === 14) {
      console.log('cedula nacional');
      this.mascara = { mask: '000-000000-00000', len: 16, userCaracters: true };
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').clearValidators();
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').setValidators(Validators.compose([Validators.required,
      Validators.maxLength(18),
      Validators.pattern('^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[a-z])$')]));
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').updateValueAndValidity();
    } else {
      console.log('cedula residente');
      this.mascara = { mask: '000000000000', len: 16, userCaracters: true };
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').clearValidators();
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').setValidators([]);
      this.formDocumentoRUC.get('_ID_REPRESENTANTE').updateValueAndValidity();
    }
  }

  get _ID_REPRESENTANTE() {
    return this.formDocumentoRUC.get('_ID_REPRESENTANTE');
  }


}
