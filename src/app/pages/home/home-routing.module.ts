import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'seleccionar-persona',
    loadChildren: () => import('./seleccionar-persona/seleccionar-persona.module').then( m => m.SeleccionarPersonaPageModule)
  },
  {
    path: 'persona-natural',
    loadChildren: () => import('./persona-natural/persona-natural.module').then( m => m.PersonaNaturalPageModule)
  },
  {
    path: 'persona-juridica',
    loadChildren: () => import('./persona-juridica/persona-juridica.module').then( m => m.PersonaJuridicaPageModule)
  },
  {
    path: 'documento-natural',
    loadChildren: () => import('./documento-natural/documento-natural.module').then( m => m.DocumentoNaturalPageModule)
  },
  {
    path: 'documento-residente',
    loadChildren: () => import('./documento-residente/documento-residente.module').then( m => m.DocumentoResidentePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
