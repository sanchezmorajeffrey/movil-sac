import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonaNaturalPage } from './persona-natural.page';

const routes: Routes = [
  {
    path: '',
    component: PersonaNaturalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonaNaturalPageRoutingModule {}
