import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PersonaNaturalPage } from './persona-natural.page';

describe('PersonaNaturalPage', () => {
  let component: PersonaNaturalPage;
  let fixture: ComponentFixture<PersonaNaturalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaNaturalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonaNaturalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
