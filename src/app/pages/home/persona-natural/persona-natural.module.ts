import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonaNaturalPageRoutingModule } from './persona-natural-routing.module';

import { PersonaNaturalPage } from './persona-natural.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonaNaturalPageRoutingModule
  ],
  declarations: [PersonaNaturalPage]
})
export class PersonaNaturalPageModule {}
