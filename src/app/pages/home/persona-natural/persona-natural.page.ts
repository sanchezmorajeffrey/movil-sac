import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-persona-natural',
  templateUrl: './persona-natural.page.html',
  styleUrls: ['./persona-natural.page.scss'],
})
export class PersonaNaturalPage implements OnInit {
  slideOpts = {
    allowSlidePrev: false,
    allowSlideNext: false,
  };
  constructor(private router: Router) { }

  ngOnInit() {
  }

  natural() {
    this.router.navigate(['/home/documento-natural']);
  }

  residente() {
    this.router.navigate(['/home/documento-residente']);
  }

}
