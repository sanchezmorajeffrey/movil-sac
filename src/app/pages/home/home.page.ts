import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  slideOpts = {
    allowSlidePrev: false,
    allowSlideNext: false,
  };

  constructor(public alertController: AlertController,
              private authenticationService: AuthenticationService, private router: Router,
              private geolocation: Geolocation) { }

  async logout() {
    const alert = await this.alertController.create({
      message: 'Desea cerrar sesión.',
      backdropDismiss: false,
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.authenticationService.logout();
        }
      }
      ]
    });
    await alert.present();
  }

  ngOnInit(): void {
    this.geolocation.getCurrentPosition().then((resp) => {
      // tslint:disable-next-line: no-unused-expression
      resp.coords.latitude;
      // tslint:disable-next-line: no-unused-expression
      resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  newSolicitud() {
    this.router.navigate(['/home/seleccionar-persona']);
  }

  editSolicitud() {
    console.log('falta');
  }

  calculadora() {
    this.router.navigate(['/calculadora']);
  }

}
