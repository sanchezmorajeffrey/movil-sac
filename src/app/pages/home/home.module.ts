import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    IonicModule,
    HomePageRoutingModule
  ],
  providers: [Geolocation],
  declarations: [HomePage]
})
export class HomePageModule { }
