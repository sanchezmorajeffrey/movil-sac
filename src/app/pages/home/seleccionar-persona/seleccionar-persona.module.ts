import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeleccionarPersonaPageRoutingModule } from './seleccionar-persona-routing.module';

import { SeleccionarPersonaPage } from './seleccionar-persona.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeleccionarPersonaPageRoutingModule
  ],
  declarations: [SeleccionarPersonaPage]
})
export class SeleccionarPersonaPageModule {}
