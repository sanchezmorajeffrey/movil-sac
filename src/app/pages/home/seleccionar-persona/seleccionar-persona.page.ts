import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seleccionar-persona',
  templateUrl: './seleccionar-persona.page.html',
  styleUrls: ['./seleccionar-persona.page.scss'],
})
export class SeleccionarPersonaPage implements OnInit {
  slideOpts = {
    allowSlidePrev: false,
    allowSlideNext: false,
  };
  constructor(private router: Router) { }

  ngOnInit() {
  }

  natural() {
    this.router.navigate(['/home/persona-natural']);
  }

  juridica() {
    this.router.navigate(['/home/persona-juridica']);
  }

}
