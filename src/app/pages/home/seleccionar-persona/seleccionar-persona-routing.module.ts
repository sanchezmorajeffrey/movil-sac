import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeleccionarPersonaPage } from './seleccionar-persona.page';

const routes: Routes = [
  {
    path: '',
    component: SeleccionarPersonaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeleccionarPersonaPageRoutingModule {}
