import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DocumentoNaturalPage } from './documento-natural.page';

describe('DocumentoNaturalPage', () => {
  let component: DocumentoNaturalPage;
  let fixture: ComponentFixture<DocumentoNaturalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoNaturalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DocumentoNaturalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
