import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PreClienteService } from 'src/app/services/pre-cliente.service';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-documento-natural',
  templateUrl: './documento-natural.page.html',
  styleUrls: ['./documento-natural.page.scss'],
})
export class DocumentoNaturalPage implements OnInit {
  formDocumentoNatural: FormGroup;
  submitted = false;
  activo = false;
  mascara = { mask: '000-000000-00000', len: 16, userCaracters: true };
  fechaNacimiento: any;
  edad: any = 18;
  documento: any;
  datosCliente: any[] = [];
  constructor(private preCliente: PreClienteService, public formBuilder: FormBuilder, private router: Router,
              private navCtrl: NavController) {
    this.formDocumentoNatural = this.formDocumentoNutural();
  }


  ngOnInit() {
  }


  private formDocumentoNutural() {
    return this.formBuilder.group({
      _IDENTIDAD: ['',
        Validators.compose([Validators.required,
        Validators.maxLength(18),
        Validators.pattern('^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[a-z])$')]),
      ],
      _PRIMER_NOMBRE: [{ value: '', disabled: false }, Validators.required],
      _SEGUNDO_NOMBRE: [{ value: '', disabled: false }],
      _PRIMER_APELLIDO: [{ value: '', disabled: false }, Validators.required],
      _SEGUNDO_APELLIDO: [{ value: '', disabled: false }],
      _MOVIL: [{ value: '', disabled: false }, Validators.required],
      _TELEFONO: [''],
      _GENERO: [{ value: '', disabled: false }, Validators.required],
      _CODIGO_ESTADO_CIVIL: [''],
      _NOMBRE_COMERCIAL: [''],
      _NOMBRE_CONTACTO: [''],
      _TELEFONO_CONTACTO: [''],
      _TIPO_CLIENTE: ['NN'],
      _DIRECCION: [''],
      _FECHA_NACIMIENTO: ['']
    });
  }

  validateUserIDNotTaken(control: any) {
    return this.preCliente.buscarClienteCedula(control).subscribe((res) => {
      this.datosCliente = res;
      console.log('res', this.datosCliente);
      if (this.datosCliente.length > 0) {
        console.log('hay coincidencia');
        this.formDocumentoNatural.get('_PRIMER_NOMBRE').setValue(this.datosCliente[0].PRIMER_NOMBRE);
        this.formDocumentoNatural.get('_PRIMER_NOMBRE').disable();
        this.formDocumentoNatural.get('_SEGUNDO_NOMBRE').setValue(this.datosCliente[0].SEGUNDO_NOMBRE);
        this.formDocumentoNatural.get('_SEGUNDO_NOMBRE').disable();
        this.formDocumentoNatural.get('_PRIMER_APELLIDO').setValue(this.datosCliente[0].PRIMER_APELLIDO);
        this.formDocumentoNatural.get('_PRIMER_APELLIDO').disable();
        this.formDocumentoNatural.get('_SEGUNDO_APELLIDO').setValue(this.datosCliente[0].SEGUNDO_APELLIDO);
        this.formDocumentoNatural.get('_SEGUNDO_APELLIDO').disable();
        this.formDocumentoNatural.get('_TELEFONO').setValue(this.datosCliente[0].TELEFONO);
        this.formDocumentoNatural.get('_MOVIL').setValue(this.datosCliente[0].MOVIL);
        this.formDocumentoNatural.get('_GENERO').setValue(this.datosCliente[0].GENERO);
        this.formDocumentoNatural.get('_GENERO').disable();
        this.activo = true;

      } else {
        console.log('no hay coincidencia');
        this.formDocumentoNatural.get('_PRIMER_NOMBRE').setValue('');
        this.formDocumentoNatural.get('_PRIMER_NOMBRE').enable();
        this.formDocumentoNatural.get('_SEGUNDO_NOMBRE').setValue('');
        this.formDocumentoNatural.get('_SEGUNDO_NOMBRE').enable();
        this.formDocumentoNatural.get('_PRIMER_APELLIDO').setValue('');
        this.formDocumentoNatural.get('_PRIMER_APELLIDO').enable();
        this.formDocumentoNatural.get('_SEGUNDO_APELLIDO').setValue('');
        this.formDocumentoNatural.get('_SEGUNDO_APELLIDO').enable();
        this.formDocumentoNatural.get('_TELEFONO').setValue('');
        this.formDocumentoNatural.get('_MOVIL').setValue('');
        this.formDocumentoNatural.get('_GENERO').setValue('');
        this.formDocumentoNatural.get('_GENERO').enable();
        this.activo = false;
      }
    });
  }


  registrarPreCliente() {
    this.submitted = true;
    if (this.formDocumentoNatural.invalid) {
      return;
    }
    this.documento = this.formDocumentoNatural.get('_IDENTIDAD').value;
    this.formDocumentoNatural.get('_FECHA_NACIMIENTO').setValue(this.fechaNacimiento);
    const data = this.formDocumentoNatural.getRawValue();
    this.preCliente.registrarPreCliente(data).subscribe((res) => {
      console.log('mira aqui!', res);
      this.navCtrl.navigateRoot(['/solicitud', res._id_pre_precliente, this.documento, 'NN']);
      // this.router.navigate(['/solicitud', res._id_pre_precliente, this.documento, 'NN']);
      // this.formDocumentoNatural.reset();
    });
    this.submitted = false;
  }



  get _IDENTIDAD() {
    return this.formDocumentoNatural.get('_IDENTIDAD');
  }


  get f() { return this.formDocumentoNatural.controls; }



  cedulaCliente(event: any) {
    event.preventDefault();
    let fecha: any = event.target.value;
    const dia = fecha.substring(4, 6);
    const mes = fecha.substring(6, 8);
    const year = fecha.substring(8, 10);
    const anioEvaluar = Number(fecha.substring(8, 10));
    console.log('año evaluar', anioEvaluar);
    if (anioEvaluar >= 20) {
      fecha = `19${year}/${mes}/${dia}`;
    } else {
      fecha = `20${year}/${mes}/${dia}`;
    }
    this.fechaNacimiento = fecha;
    Date.parse(this.fechaNacimiento);
    console.log('fecha', this.fechaNacimiento);
    this.calcularEdad(this.fechaNacimiento);
    this.validateUserIDNotTaken(event.target.value);
  }


  calcularEdad(fecha: any) {
    const hoy = new Date();
    const cumpleanos = new Date(fecha);
    this.edad = hoy.getFullYear() - cumpleanos.getFullYear();
    const m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      this.edad--;
    }

    return console.log('edad', this.edad);
  }



}
