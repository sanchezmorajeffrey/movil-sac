import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentoNaturalPageRoutingModule } from './documento-natural-routing.module';

import { DocumentoNaturalPage } from './documento-natural.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BrMaskerModule,
    DocumentoNaturalPageRoutingModule
  ],
  declarations: [DocumentoNaturalPage]
})
export class DocumentoNaturalPageModule {}
