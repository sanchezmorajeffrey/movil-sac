import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentoNaturalPage } from './documento-natural.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentoNaturalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentoNaturalPageRoutingModule {}
