import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { PreSolicitudService } from 'src/app/services/pre-solicitud.service';
import { Storage } from '@ionic/storage';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.page.html',
  styleUrls: ['./solicitud.page.scss'],
})
export class SolicitudPage implements OnInit {
  idPreCliente: any;
  formSolicitud: FormGroup;
  submitted = false;
  dataDestinoCredito = [];
  dataProducto = [];
  dataFrecuencia = [];
  documento: any;
  textBtn = 'Siguiente';
  valor: any = false;
  tipoPersona: any;


  constructor(private router: Router, private route: ActivatedRoute,
              public formBuilder: FormBuilder,
              public solicitud: PreSolicitudService,
              public alertController: AlertController,
              private storage: Storage, private navCtrl: NavController) {
    this.formSolicitud = this.createFormSolicitud();

  }


  ngOnInit() {
    this.route.params.subscribe(param => {
      this.idPreCliente = param.id;
      this.documento = param.documento;
      this.tipoPersona = param.tipo;
    });
    console.log('idPreCliente', this.idPreCliente);
    console.log('documento', this.documento);
    console.log('tipo persona', this.tipoPersona);
    this.cargarDestinoCredito();
    this.cargarDatosProducto();
    this.cargarDatosFrecuencia();
    console.log('valor inicial', this.valor);
  }


  private createFormSolicitud() {
    return this.formBuilder.group({
      _MONTO: ['', Validators.required],
      _PLAZO: ['', Validators.required],
      _ID_PRODUCTO: ['', Validators.required],
      _ID_FRECUENCIA: ['', Validators.required],
      _IDENTIDAD: [''],
      _FECHA_SOLICITUD: [''],
      _ID_DESTINO_CREDITO: ['', Validators.required],
      _INTERES_CORRIENTE: [''],
      _MONTO_PAGAR: ['', Validators.required],
      _ID_PRE_CLIENTE: [''],
      _AUTORIZA_CONSULTA: [false]
    });
  }


  guardarSolicitud() {
    this.submitted = true;
    if (this.formSolicitud.invalid) {
      return;
    }
    console.log('hola');
    this.formSolicitud.get('_ID_PRE_CLIENTE').setValue(this.idPreCliente);
    const data = this.formSolicitud.value;
    if (this.valor === true) {
      this.solicitud.registrarPreSolicitud(data).subscribe((res) => {
        console.log('mira aqui!', res);
        this.guardarIdSolicitud(res._id_pre_solicitud);
        this.navCtrl.navigateRoot(['/resultado', this.idPreCliente, this.documento, this.tipoPersona]);
        // this.formSolicitud.reset();
      });
    } else {
      console.log('hola');
      this.presentAlertConfirm(data);
      // this.solicitud.registrarPreSolicitud(data).subscribe((res) => {
      //   console.log('mira aqui!', res);
      //   this.router.navigate(['/home']);
      //   this.formSolicitud.reset();
      // });
    }

    this.submitted = false;
  }


  cargarDestinoCredito() {
    this.solicitud.cargarDistinoCredito().subscribe((res) => {
      this.dataDestinoCredito = res;
      console.log('destino crédito', this.dataDestinoCredito);
    });
  }


  get inputPlazo() {
    return this.formSolicitud.get('_PLAZO');
  }


  cargarDatosFrecuencia() {
    this.solicitud.cargarDatosFrecuencia().subscribe((res) => {
      this.dataFrecuencia = res;
      console.log('frecuencia', this.dataFrecuencia);
    });
  }


  cargarDatosProducto() {
    this.solicitud.cargarDatosProducto().subscribe((res) => {
      this.dataProducto = res;
      console.log('producto', this.dataProducto);
    });
  }


  ionChange(event: any) {
    console.log(event.detail.checked);
    this.valor = event.detail.checked;

    if (this.valor) {
      console.log(this.valor, 'true');
      this.textBtn = 'SIGUIENTE';
      this.formSolicitud.get('_ID_FRECUENCIA').clearValidators();
      this.formSolicitud.get('_ID_FRECUENCIA').setValidators([Validators.required]);
      this.formSolicitud.get('_ID_FRECUENCIA').updateValueAndValidity();

      // this.formSolicitud.get('_INTERES_CORRIENTE').clearValidators();
      // this.formSolicitud.get('_INTERES_CORRIENTE').setValidators([Validators.required]);
      // this.formSolicitud.get('_INTERES_CORRIENTE').updateValueAndValidity();

      this.formSolicitud.get('_PLAZO').clearValidators();
      this.formSolicitud.get('_PLAZO').setValidators(Validators.compose([Validators.required, Validators.pattern('^[0-9]+')]));
      this.formSolicitud.get('_PLAZO').updateValueAndValidity();

      this.formSolicitud.get('_MONTO_PAGAR').clearValidators();
      this.formSolicitud.get('_MONTO_PAGAR').setValidators([Validators.required]);
      this.formSolicitud.get('_MONTO_PAGAR').updateValueAndValidity();
    } else {
      console.log(this.valor, 'false');
      this.textBtn = 'FINALIZAR';
      this.formSolicitud.get('_ID_FRECUENCIA').clearValidators();
      this.formSolicitud.get('_ID_FRECUENCIA').setValidators([]);
      this.formSolicitud.get('_ID_FRECUENCIA').updateValueAndValidity();

      // this.formSolicitud.get('_INTERES_CORRIENTE').clearValidators();
      // this.formSolicitud.get('_INTERES_CORRIENTE').setValidators([]);
      // this.formSolicitud.get('_INTERES_CORRIENTE').updateValueAndValidity();

      this.formSolicitud.get('_PLAZO').clearValidators();
      this.formSolicitud.get('_PLAZO').setValidators([]);
      this.formSolicitud.get('_PLAZO').updateValueAndValidity();

      this.formSolicitud.get('_MONTO_PAGAR').clearValidators();
      this.formSolicitud.get('_MONTO_PAGAR').setValidators([]);
      this.formSolicitud.get('_MONTO_PAGAR').updateValueAndValidity();
    }
  }


  get f() { return this.formSolicitud.controls; }


  async presentAlertConfirm(data: any) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.solicitud.registrarPreSolicitud(data).subscribe((res) => {
              console.log('mira aqui!', res);
              this.navCtrl.navigateRoot(['/home']);
              this.formSolicitud.reset();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async guardarIdSolicitud(idSolicitud: string) {
    await this.storage.set('idSolicitud', idSolicitud);
  }


}
