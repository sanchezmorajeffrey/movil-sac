import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConsultaSinRiesgoService } from 'src/app/services/consulta-sin-riesgo.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { NavController, } from '@ionic/angular';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {
  documento: string;
  dataClienteAnalisis: any;
  lat: number;
  log: number;
  idPre: any;
  tipoPersona: any;
  idPresolicitud: any;
  idSeccionContinua: any;

  constructor(private ruta: ActivatedRoute, private router: Router, private consulta: ConsultaSinRiesgoService,
              private geolocation: Geolocation, private storage: Storage, private navCtrl: NavController) {
  }

  ngOnInit() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('latitud:', resp.coords.latitude);
      console.log('longitud:', resp.coords.longitude);
      this.lat = resp.coords.latitude;
      this.log = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    this.ruta.params.subscribe(param => {
      this.documento = param.documento;
      this.idPre = param.pre;
      this.tipoPersona = param.tipo;
    });
    this.consulta.consultaRecord(this.documento, 'F', 1).subscribe((res) => {
      console.log('res', res);
      this.dataClienteAnalisis = res.scoringInterno[0];
      const fd = { latitud: 0 };
      const ft = { longitud: 0 };
      const fa = { id_pre_solicitud: this.idPre };
      Object.assign(this.dataClienteAnalisis, fd);
      Object.assign(this.dataClienteAnalisis, ft);
      Object.assign(this.dataClienteAnalisis, fa);
      this.consulta.guardarConsultaRecord(this.dataClienteAnalisis).subscribe((respuesta) => {
        console.log('Mensaje', respuesta);
      });
    });
    this.cargarIdPreSolicitud();
  }

  actividades() {
    this.consulta.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((res) => {
      console.log('talve aqui obtenco la seccion a rendierizar', res);
      this.idSeccionContinua =  res[0].ID_SECCION;
      console.log('idSeccionContinuacion', this.idSeccionContinua);
      switch (this.idSeccionContinua) {
        case  1:
          // console.log('id', this.idSeccionContinua);
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case  2:
          this.navCtrl.navigateRoot(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        case  4:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        default:
          break;
      }

    });
  }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
  }

  home() {
    this.storage.remove('idSolicitud');
    this.navCtrl.navigateRoot(['/home']);
  }

}
