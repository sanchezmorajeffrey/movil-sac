import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EconomiaFamiliarPage } from './economia-familiar.page';

const routes: Routes = [
  {
    path: '',
    component: EconomiaFamiliarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EconomiaFamiliarPageRoutingModule {}
