import { Component, OnInit } from '@angular/core';
import { ActividadesService } from 'src/app/services/actividades.service';
import { NavController, } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InformacionGeneralService } from 'src/app/services/informacion-general.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-economia-familiar',
  templateUrl: './economia-familiar.page.html',
  styleUrls: ['./economia-familiar.page.scss'],
})
export class EconomiaFamiliarPage implements OnInit {
  tipoPersona: any;
  submitted = false;
  idSeccion: any;
  formEconomiaFamiliar: FormGroup;
  idPresolicitud: any;
  preguntasEconomia: any[] = [];
  idSeccionContinua: any;
  esAsalariado = false;
  otraPersonaAporta = false;
  tieneAlquileres = false;
  tarjetaCredito = false;

  esCasado = false;
  aportaEsposa = false;
  alquilerMostrar = false;
  mantenimientoCasaMostrar = false;

  constructor(private router: Router, private storage: Storage,
              private navCtrl: NavController,
              private actividad: ActividadesService,
              private route: ActivatedRoute,
              public formBuilder: FormBuilder,
              public alertController: AlertController,
              public informacion: InformacionGeneralService) {
    this.formEconomiaFamiliar = this.createMyFormEconomiaFamiliar();
  }



  ngOnInit() {
    this.route.params.subscribe(param => {
      this.tipoPersona = param.tipo;
      this.idSeccion = param.idSeccion;
    });
    console.log('tipo persona', this.tipoPersona);
    this.cargarIdPreSolicitud();
  }

  createMyFormEconomiaFamiliar() {
    return this.formBuilder.group({
      _ES_ASALARIADO: ['', Validators.required],
      _NOMBRE_LUGAR_TRABAJA: [''],
      _TIEMPO_TRABAJAR_LUGAR: [''],
      _CARGO_DESEMPENA_LUGAR: [''],
      _MONTO_LUGAR: [''],

      _CONYUGUE_TRABAJO: [''],
      _APORTAR_CONYUGUE: [''],
      _OTRA_PERSONA: ['', Validators.required],
      _ALQUILER_TERENO: ['', Validators.required],
      _MONTO_OTRA_PERSONA: [''],
      _MONTO_ALQUILER_TERENO: [''],

      _GASTO_SEMANA_COMIDA: ['', Validators.required],
      _GASTO_MENSUAL_EDUCACION: ['', Validators.required],
      _GASTO_ROPA_ZAPATO: ['', Validators.required],
      _GASTO_SALUD_MENSUAL: ['', Validators.required],

      _CONCEPTO_ALQUILER: [''],
      _PROMEDIO_MANTENIMIENTO_CASA: [''],
      _MONTO_SERVICIOS_BASICO: ['', Validators.required],

      _GASTO_MENSUAL_TRANSPORTE: ['', Validators.required],
      _EMPLEADA_SALARIO: ['', Validators.required],
      _BRINDA_AYUDA_A_FAMILIARES: ['', Validators.required],
      _TARJETA_CREDITA: ['', Validators.required],
      _SALDO_TARJETA_CREDITO: [''],
      _PAGO_MINIMO_TARJETA: [''],
      _CUOTA_CASA_COMERCIAL: ['', Validators.required],
      _CUOTA_BANCO: ['', Validators.required],
      _OTROS_INGRESOS_MENSUALES: ['', Validators.required],
      _OTROS_GASTOS_MENSUALES: ['', Validators.required]
    });
  }

  get f() { return this.formEconomiaFamiliar.controls; }

  async cargarIdPreSolicitud() {
    this.idPresolicitud = await this.storage.get('idSolicitud') || null;
    this.obtenerPreguntas();
  }

  obtenerPreguntas() {
    this.informacion.obtenerSeccion(this.idPresolicitud, this.idSeccion).subscribe((res) => {
      this.preguntasEconomia = res;
      console.log('preguntas economia', this.preguntasEconomia);
      this.preguntasEconomia.forEach(element => {
        if (element.ID_CUESTIONARIO === 43) {
          if (element.VISEBLE === false) {
            this.esCasado = false;
          } else {
            this.esCasado = true;
          }
        }
        if (element.ID_CUESTIONARIO === 55) {
          if (element.VISEBLE === false) {
            this.alquilerMostrar = false;
          } else {
            this.alquilerMostrar = true;
          }
        }
        if (element.ID_CUESTIONARIO === 56) {
          if (element.VISEBLE === false) {
            this.mantenimientoCasaMostrar = false;
          } else {
            this.mantenimientoCasaMostrar = true;
          }
        }
      });
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Terminar',
      backdropDismiss: false,
      message: 'Esta seguro de terminar el <strong>proceso?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.navigateRoot(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }


  changeAsalariado(event: any) {
    console.log('asalariado', event.target.value);
    const asalariado = event.target.value;

    if (asalariado === 'Sí') {
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').reset();
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').clearValidators();
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').reset();
      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').clearValidators();
      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').reset();
      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').clearValidators();
      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_MONTO_LUGAR').reset();
      this.formEconomiaFamiliar.get('_MONTO_LUGAR').clearValidators();
      this.formEconomiaFamiliar.get('_MONTO_LUGAR').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_MONTO_LUGAR').updateValueAndValidity();
      this.esAsalariado = true;
    } else {
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').reset();
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').clearValidators();
      this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').reset();
      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').clearValidators();

      this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').reset();
      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').clearValidators();

      this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_MONTO_LUGAR').reset();
      this.formEconomiaFamiliar.get('_MONTO_LUGAR').clearValidators();

      this.formEconomiaFamiliar.get('_MONTO_LUGAR').updateValueAndValidity();
      this.esAsalariado = false;
    }
  }

  changeConyugueSalario(event: any) {
    console.log('conyugue', event.target.value);
    const aporta = event.target.value;

    if (aporta === 'Sí') {
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').reset();
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').clearValidators();
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').updateValueAndValidity();
      this.aportaEsposa = true;
    } else {
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').reset();
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').clearValidators();
      this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').updateValueAndValidity();
      this.aportaEsposa = false;
    }

  }


  changeTarjetaCredito(event: any) {
    console.log('tarjeta', event.target.value);
    const tarjeta = event.target.value;
    if (tarjeta === 'Sí') {
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').reset();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').clearValidators();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').reset();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').clearValidators();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').updateValueAndValidity();
      this.tarjetaCredito = true;
    } else {
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').reset();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').clearValidators();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').updateValueAndValidity();

      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').reset();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').clearValidators();
      this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').updateValueAndValidity();
      this.tarjetaCredito = false;
    }
  }



  changeOtraPersonaAporta(event: any) {
    console.log('PersonaAporta', event.target.value);


    const aportaPersona = event.target.value;

    if (aportaPersona === 'Sí') {
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').reset();
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').clearValidators();
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').updateValueAndValidity();
      this.otraPersonaAporta = true;
    } else {
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').reset();
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').clearValidators();
      this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').updateValueAndValidity();
      this.otraPersonaAporta = false;
    }
  }

  changeIngresoPorTerreno(event: any) {
    console.log('IngresoPorTerreno', event.target.value);
    const alquilerTerreno = event.target.value;
    if (alquilerTerreno === 'Sí') {
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').reset();
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').clearValidators();
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').setValidators([Validators.required]);
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').updateValueAndValidity();
      this.tieneAlquileres = true;
    } else {
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').reset();
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').clearValidators();
      this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').updateValueAndValidity();
      this.tieneAlquileres = false;
    }
  }

  guardarEconomiaFamiliar() {
    this.submitted = true;
    if (this.formEconomiaFamiliar.invalid) {
      return;
    }
    const data = [
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[0].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_ES_ASALARIADO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[1].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_NOMBRE_LUGAR_TRABAJA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[2].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_TIEMPO_TRABAJAR_LUGAR').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[3].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_CARGO_DESEMPENA_LUGAR').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[4].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_MONTO_LUGAR').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[5].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_CONYUGUE_TRABAJO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[6].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_APORTAR_CONYUGUE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[7].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_OTRA_PERSONA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[8].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_ALQUILER_TERENO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[9].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_MONTO_OTRA_PERSONA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[10].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_MONTO_ALQUILER_TERENO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[11].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_GASTO_SEMANA_COMIDA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[12].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_GASTO_MENSUAL_EDUCACION').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[13].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_GASTO_ROPA_ZAPATO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[14].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_GASTO_SALUD_MENSUAL').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[15].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_CONCEPTO_ALQUILER').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[16].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_PROMEDIO_MANTENIMIENTO_CASA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[17].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_MONTO_SERVICIOS_BASICO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[18].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_GASTO_MENSUAL_TRANSPORTE').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[19].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_EMPLEADA_SALARIO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[20].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_BRINDA_AYUDA_A_FAMILIARES').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[21].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_SALDO_TARJETA_CREDITO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[22].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_PAGO_MINIMO_TARJETA').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[23].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_CUOTA_CASA_COMERCIAL').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[24].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_CUOTA_BANCO').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[25].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_OTROS_INGRESOS_MENSUALES').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[26].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_OTROS_GASTOS_MENSUALES').value,
      },
      {
        _ID_PRE_SOLICITUD: this.idPresolicitud,
        _ID_CUESTIONARIO: this.preguntasEconomia[27].ID_CUESTIONARIO,
        _REPUESTA: this.formEconomiaFamiliar.get('_TARJETA_CREDITA').value,
      }
    ];
    console.log('esto envia', data);

    this.actividad.guardarRespuestaActividad(data).subscribe((res) => {
      console.log('respuesta de guardado', res);
    });
    this.actividad.guardarSeccionTerminada(this.idPresolicitud, this.idSeccion).subscribe((resSeccionTerminada) => {
      console.log('guardado de seccion terminada', resSeccionTerminada);
    });
    console.log('id seccion dentro del metodo guardar');
    this.actividad.guardarSeccionSolicitud(this.idPresolicitud, this.tipoPersona).subscribe((resSaber) => {
      console.log('endpoint para saber la siguiente id', resSaber);
      this.idSeccionContinua = resSaber[0].ID_SECCION;
      switch (this.idSeccionContinua) {
        case 1:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 2:
          this.navCtrl.navigateRoot(['/informacion-persona/', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 3:
          this.navCtrl.navigateRoot(['/informacion-persona-juridica', this.tipoPersona, this.idSeccionContinua]);
          break;
        case 4:
          this.navCtrl.navigateRoot(['/actividades', this.tipoPersona]);
          break;
        case 5:
          this.navCtrl.navigateRoot(['/informacion-negocio-natural', this.tipoPersona, this.idSeccionContinua]);
          break;
        default:
          break;
      }
    });
  }
}
