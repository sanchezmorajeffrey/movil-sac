import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EconomiaFamiliarPageRoutingModule } from './economia-familiar-routing.module';

import { EconomiaFamiliarPage } from './economia-familiar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    IonicModule,
    EconomiaFamiliarPageRoutingModule
  ],
  declarations: [EconomiaFamiliarPage]
})
export class EconomiaFamiliarPageModule {}
