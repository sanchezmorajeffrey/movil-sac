import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  myFormCalculadora: FormGroup;
  monto: number;
  dataProducto: any;
  dataAseguradora: any;
  // tslint:disable-next-line: variable-name
  codigo_producto: any;
  plazo: number;
  // tslint:disable-next-line: variable-name
  codigo_aseguradora: any;
  montoCuota = 0;
  montoMinimo: number;
  montoMaximo: number;
  montoActual: number;
  montoInicio = 0;
  numCuotas: number;
  plazoMaximo: number;
  plazoMinimo: number;
  nombreProducto: string;
  montoActualInput: number;
  plazoActualInput: number;
  montoActualRange: number;
  idProductoSeleccionado: number;
  auxiliar: any;

  constructor(
    public calculadora: CalculadoraService,
    public formBuilder: FormBuilder,
    public alertController: AlertController
  ) {
    this.myFormCalculadora = this.createMyFormCalculadora();
  }


  ngOnInit() {
    this.cargarProducto();
    this.cargarAseguradora();
  }


  private createMyFormCalculadora() {
    return this.formBuilder.group({
      codigo_producto: ['', Validators.required],
      monto: ['', Validators.required],
      plazo: ['', Validators.compose([
        Validators.required, Validators.max(Number(this.plazoMaximo)), Validators.min(Number(this.plazoMinimo))
      ])],
      codigo_aseguradora: ['', Validators.required],
    });
  }


  async calcularCuota() {
    this.codigo_producto = this.myFormCalculadora.get('codigo_producto').value;
    this.monto = Number(this.myFormCalculadora.get('monto').value);
    this.plazo = this.myFormCalculadora.get('plazo').value;
    this.codigo_aseguradora = this.myFormCalculadora.get('codigo_aseguradora').value;
    if (this.monto === 0 || this.plazo === 0) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Verifique los datos`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Ok');
            }
          }
        ]
      });
      return await alert.present();
    }

    if (this.monto < this.montoMinimo || this.monto > this.montoMaximo) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Verifique los datos`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Ok');
            }
          }
        ]
      });
      return await alert.present();
    }

    if (this.plazo < this.plazoMinimo || this.plazo > this.plazoMaximo) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Verifique los datos`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Ok');
            }
          }
        ]
      });
      return await alert.present();
    }
    console.log(this.myFormCalculadora.value);
    this.calculadora.getCuota(this.monto, this.codigo_producto, this.plazo, this.codigo_aseguradora).subscribe((res) => {
      this.montoCuota = res[0].Total;
    });
  }


  onChange($event: any) {
    this.idProductoSeleccionado = $event.target.value;
    this.auxiliar = this.dataProducto;
    this.auxiliar = this.dataProducto.filter(x => x.ID_PRODUCTO === this.idProductoSeleccionado);
    this.montoMaximo = this.auxiliar[0].MONTO_MAXIMO;
    this.montoMinimo = this.auxiliar[0].MONTO_MINIMO;
    this.montoInicio = this.auxiliar[0].MONTO_MAXIMO;
    this.numCuotas = this.auxiliar[0].NUM_CUOTAS;
    this.plazoMinimo = this.auxiliar[0].PLAZO_MINIMO;
    this.plazoMaximo = this.auxiliar[0].PLAZO_MAXIMO;
    this.nombreProducto = this.auxiliar[0].NOMBRE_PRODUCTO;
    this.montoCuota = 0;
  }


  ionChange1($event: any) {
    this.montoActual = $event.target.value;
    this.montoActual = Number(this.montoActual);
  }



  aumentar() {
    this.montoInicio = this.montoActual + 1000;
  }



  restar() {
    this.montoInicio = this.montoActual - 1000;
  }


  async ionBlur($event: any) {
    console.log($event.target.value);
    this.montoActualInput = $event.target.value;
    this.montoActualInput = Number(this.montoActualInput);
    if (this.montoActualInput < this.montoMinimo) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Fuera de Rango Monto: ${this.montoActualInput}`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
            }
          }
        ]
      });
      await alert.present();
    }

    if (this.montoActualInput > this.montoMaximo) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Fuera de Rango Monto: ${this.montoActualInput}`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
            }
          }
        ]
      });

      await alert.present();
    }
  }


  async ionBlur1($event: any) {
    console.log($event.target.value);
    this.plazoActualInput = $event.target.value;

    if (this.plazoActualInput > this.plazoMaximo) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Fuera de Rango Plazo: ${this.plazoActualInput}`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
            }
          }
        ]
      });
      await alert.present();
    }

    if (this.plazoActualInput === 0) {
      this.montoCuota = 0;
      const alert = await this.alertController.create({
        message: `Fuera de Rango Plazo: ${this.plazoActualInput}`,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
            }
          }
        ]
      });
      await alert.present();
    }
  }


  cargarProducto() {
    this.calculadora.getProductos().subscribe((res) => {
      this.dataProducto = res;
      console.log('dataProducto', this.dataProducto);
    });
  }


  cargarAseguradora() {
    this.calculadora.getAseguradora().subscribe((res) => {
      this.dataAseguradora = res;
    });
  }

}
