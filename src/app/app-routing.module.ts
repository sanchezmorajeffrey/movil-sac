import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UsuarioGuard } from './guards/usuario.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'calculadora',
    loadChildren: () => import('./pages/calculadora/calculadora.module').then(m => m.CalculadoraPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'resultado/:pre/:documento/:tipo',
    loadChildren: () => import('./pages/resultado/resultado.module').then(m => m.ResultadoPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'solicitud/:id/:documento/:tipo',
    loadChildren: () => import('./pages/solicitud/solicitud.module').then(m => m.SolicitudPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'actividades/:tipo/:idSeccion',
    loadChildren: () => import('./pages/actividades/actividades.module').then(m => m.ActividadesPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'informacion-persona/:tipo/:idSeccion',
    loadChildren: () => import('./pages/informacion-persona/informacion-persona.module').then(m => m.InformacionPersonaPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'informacion-persona-juridica/:tipo/:idSeccion',
    loadChildren: () => import('./pages/informacion-persona-juridica/informacion-persona-juridica.module').
      then(m => m.InformacionPersonaJuridicaPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'informacion-negocio-juridico/:tipo/:idSeccion',
    loadChildren: () => import('./pages/informacion-negocio-juridico/informacion-negocio-juridico.module')
      .then(m => m.InformacionNegocioJuridicoPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'informacion-negocio/:tipo/:idSeccion',
    loadChildren: () => import('./pages/informacion-negocio/informacion-negocio.module').
      then(m => m.InformacionNegocioPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'economia-familiar/:tipo/:idSeccion',
    loadChildren: () => import('./pages/economia-familiar/economia-familiar.module').
      then(m => m.EconomiaFamiliarPageModule),
    canLoad: [UsuarioGuard]
  },
  {
    path: 'informacion-negocio-natural/:tipo/:idSeccion',
    loadChildren: () => import('./pages/informacion-negocio-natural/informacion-negocio-natural.module').
      then(m => m.InformacionNegocioNaturalPageModule),
    canLoad: [UsuarioGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
